package com.lottery.lotterysystem.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lottery.lotterysystem.entityListener.LotteryEventEntityListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(LotteryEventEntityListener.class)
@Table(name = "lottery_event")
public class LotteryEventEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "winner_ticket_id")
    @JsonBackReference
    private TicketEntity winnerTicketEntity;

    @ManyToOne
    @JoinColumn(name = "lottery_id")
    private LotteryEntity lotteryEntity;

    @ManyToOne
    @JoinColumn(name = "event_status_id")
    private EventStatusEntity eventStatusEntity;

    @OneToMany(mappedBy = "lotteryEventEntity")
    @JsonIgnore
    private List<TicketEntity> ticketEntities;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreationTimestamp
    @JsonIgnore
    private Date createdDate;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @JsonIgnore
    private Date modifiedDate;
}
