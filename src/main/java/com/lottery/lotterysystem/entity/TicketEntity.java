package com.lottery.lotterysystem.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ticket", uniqueConstraints =
@UniqueConstraint(columnNames = {"lottery_event_id", "user_id"}))
public class TicketEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private TicketStatusEntity status;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "lottery_event_id")
    private LotteryEventEntity lotteryEventEntity;

    @OneToOne(mappedBy = "ticketEntity", cascade = CascadeType.REMOVE)
    private PaymentTransactionEntity paymentTransactionEntity;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @OneToOne
    @JoinColumn(name = "email_id")
    private EmailEntity emailEntity;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreationTimestamp
    @JsonIgnore
    private Date createdDate;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @JsonIgnore
    private Date modifiedDate;
}
