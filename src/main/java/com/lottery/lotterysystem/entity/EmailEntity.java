package com.lottery.lotterysystem.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "email")
public class EmailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private TicketEntity ticketEntity;

    private String toEmail;

    private String subject;

    private String content;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreationTimestamp
    @JsonIgnore
    private Date createdDate;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @JsonIgnore
    private Date modifiedDate;
}
