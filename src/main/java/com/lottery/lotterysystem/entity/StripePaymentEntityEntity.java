package com.lottery.lotterysystem.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "stripe_payment",
        indexes = {@Index(columnList = "client_secret", unique = true), @Index(columnList = "payment_intent", unique = true)})
public class StripePaymentEntityEntity extends PaymentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "client_secret")
    private String clientSecret;

    @Column(name = "payment_intent")
    private String paymentIntent;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreationTimestamp
    @JsonIgnore
    private Date createdDate;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @JsonIgnore
    private Date modifiedDate;
}
