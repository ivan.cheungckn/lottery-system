package com.lottery.lotterysystem.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.InheritanceType.JOINED;

@Entity
@Inheritance(strategy = JOINED)
@Getter
@Setter
@Table(name = "payment")
public abstract class PaymentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "payment_transaction_id", referencedColumnName = "id")
    private PaymentTransactionEntity paymentTransactionEntity;
}
