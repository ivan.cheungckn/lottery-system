package com.lottery.lotterysystem.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "lottery")
public class LotteryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "interval_second")
    private Long intervalSecond;
    @Column(name = "start_time")
    private Date startTime;
    @Column(name = "end_time")
    private Date endTime;
    private String name;
    private String description;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @OneToMany(mappedBy = "lotteryEntity")
    @JsonIgnore
    private List<LotteryEventEntity> lotteryEventEntities;

    @ManyToOne
    @JoinColumn(name = "draw_strategy_id")
    @JsonIgnore
    private DrawStrategyEntity drawStrategyEntity;

    @Column(name = "ticket_price")
    private Long ticketPrice;

    @Column(name = "created_date", nullable = false, updatable = false)
    @CreationTimestamp
    @JsonIgnore
    private Date createdDate;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @JsonIgnore
    private Date modifiedDate;
}
