package com.lottery.lotterysystem.util.DrawStrategy;

import com.lottery.lotterysystem.entity.LotteryEventEntity;
import com.lottery.lotterysystem.entity.TicketEntity;
import com.lottery.lotterysystem.service.ticket.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;

@Service
@Transactional
public class RandomDrawStrategy extends AbstractDrawStrategy {
    private TicketService ticketService;

    @Autowired
    public RandomDrawStrategy(TicketService ticketService) {
        this.ticketService = ticketService;
        super.setType("random");
    }

    @Override
    public LotteryEventEntity drawWinner(LotteryEventEntity lotteryEventEntity) {

        Random rand = new Random();
        List<TicketEntity> ticketEntities = ticketService.countAllCompleteTransactionByEventId(lotteryEventEntity.getId());

        if (!ticketEntities.isEmpty()) {
            TicketEntity winnerTicketEntity = ticketEntities.get(rand.nextInt(ticketEntities.size()));
            lotteryEventEntity.setWinnerTicketEntity(winnerTicketEntity);
        }
        return lotteryEventEntity;
    }
}
