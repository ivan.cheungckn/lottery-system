package com.lottery.lotterysystem.util.DrawStrategy;

import com.lottery.lotterysystem.entity.LotteryEventEntity;

public abstract class AbstractDrawStrategy {
    private String type;

    public abstract LotteryEventEntity drawWinner(LotteryEventEntity lotteryEventEntity);

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
