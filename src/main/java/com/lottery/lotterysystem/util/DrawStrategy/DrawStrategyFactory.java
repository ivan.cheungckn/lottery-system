package com.lottery.lotterysystem.util.DrawStrategy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
public class DrawStrategyFactory {

    final Logger log = LogManager.getLogger(DrawStrategyFactory.class);
    private List<AbstractDrawStrategy> abstractDrawStrategyList;

    private static Map<String, AbstractDrawStrategy> drawStrategyMap = new HashMap<>();

    @PostConstruct
    public void initDrawStrategyMap() {
        for (AbstractDrawStrategy drawStrategy : abstractDrawStrategyList) {
            drawStrategyMap.put(drawStrategy.getType(), drawStrategy);
        }
    }

    public static AbstractDrawStrategy getDrawStrategy(String type) {
        AbstractDrawStrategy drawStrategy = drawStrategyMap.get(type);
        if (Objects.isNull(drawStrategy)) throw new RuntimeException("Unknown draw strategy type: " + type);
        return drawStrategy;
    }

    @Autowired
    public void setAbstractDrawStrategyList(List<AbstractDrawStrategy> abstractDrawStrategyList) {
        this.abstractDrawStrategyList = abstractDrawStrategyList;
    }

}
