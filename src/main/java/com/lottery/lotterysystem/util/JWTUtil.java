package com.lottery.lotterysystem.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.lottery.lotterysystem.entity.RoleEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class JWTUtil {

    @Value("${jwt.secret}")
    private String secret;

    public static String generateAccessToken(User user, String issuer) {
        return generateAccessToken(user.getUsername(), issuer, user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()));
    }

    public static String generateAccessToken(com.lottery.lotterysystem.entity.UserEntity userEntity, String issuer) {
        return generateAccessToken(userEntity.getUsername(), issuer, userEntity.getRoleEntities().stream().map(RoleEntity::getName).collect(Collectors.toList()));
    }

    public static String generateAccessToken(String username, String issuer, List<String> roles) {
        Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
        return JWT.create().withSubject(username)
                .withExpiresAt(new Date(System.currentTimeMillis() + 60 * 60 * 1000))
                .withIssuer(issuer)
                .withClaim("roles", roles)
                .sign(algorithm);
    }

    public static String generateRefreshToken(User user, String issuer) {
        Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
        return JWT.create().withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + 240 * 60 * 1000))
                .withIssuer(issuer)
                .sign(algorithm);
    }

    public static DecodedJWT verifyToken(String token) throws Exception {
        Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
        JWTVerifier jwtVerifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = jwtVerifier.verify(token);
        return decodedJWT;
    }
}
