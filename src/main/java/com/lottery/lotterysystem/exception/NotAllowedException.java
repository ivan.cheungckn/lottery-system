package com.lottery.lotterysystem.exception;

public class NotAllowedException extends Exception {
    public NotAllowedException(String message) {
        super(message);
    }
}

