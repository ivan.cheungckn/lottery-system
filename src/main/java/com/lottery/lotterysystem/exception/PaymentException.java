package com.lottery.lotterysystem.exception;

public class PaymentException extends Exception {
    public PaymentException(String s) {
        super(s);
    }
}
