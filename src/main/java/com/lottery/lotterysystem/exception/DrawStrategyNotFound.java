package com.lottery.lotterysystem.exception;

public class DrawStrategyNotFound extends Exception {
    public DrawStrategyNotFound(String message) {
        super(message);
    }
}
