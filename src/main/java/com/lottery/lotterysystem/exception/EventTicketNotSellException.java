package com.lottery.lotterysystem.exception;

public class EventTicketNotSellException extends Exception {
    public EventTicketNotSellException(String s) {
        super(s);
    }
}
