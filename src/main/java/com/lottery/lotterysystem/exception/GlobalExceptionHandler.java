package com.lottery.lotterysystem.exception;

import com.lottery.lotterysystem.dto.ErrorResponse;
import org.slf4j.MDC;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.lottery.lotterysystem.constant.CoreConstants.TRANSACTION_ID_FOR_LOG;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
//    private final Logger LOG = LogManager.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler({UnauthorizedException.class})
    public ResponseEntity<ErrorResponse> handleUnauthorizedException(Exception e, WebRequest request) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(buildErrorResponse(e));
    }


    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorResponse> handleGeneralException(Exception e, WebRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(buildErrorResponse(e));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException e, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        return ResponseEntity.badRequest().body(buildErrorResponse(e));
    }


    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception e, @Nullable Object body, HttpHeaders headers,
                                                             HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute("javax.servlet.error.exception", e, 0);
        }
        return new ResponseEntity<Object>(buildErrorResponse(e), headers, status);
    }

    public ErrorResponse buildErrorResponse(Exception e) {
        return buildErrorResponse(e.getMessage());
    }

    public ErrorResponse buildErrorResponse(String message) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setMessage(message);
        errorResponse.setTraceId(MDC.get(TRANSACTION_ID_FOR_LOG));
        return errorResponse;
    }
}
