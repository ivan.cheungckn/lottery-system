package com.lottery.lotterysystem.dto;

import lombok.Data;

@Data
public class StripeResponseData {
    private boolean success;
    private String clientSecret;
}
