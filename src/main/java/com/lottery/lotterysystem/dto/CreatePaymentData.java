package com.lottery.lotterysystem.dto;

import com.fasterxml.jackson.annotation.JsonAlias;

public class CreatePaymentData {
    @JsonAlias("items")
    Object[] items;

    public Object[] getItems() {
        return items;
    }
}
