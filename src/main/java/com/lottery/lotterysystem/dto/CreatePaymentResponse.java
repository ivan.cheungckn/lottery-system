package com.lottery.lotterysystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreatePaymentResponse {
    private boolean success;
    private String clientSecret;
}