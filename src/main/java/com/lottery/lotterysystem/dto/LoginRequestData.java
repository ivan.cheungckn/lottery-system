package com.lottery.lotterysystem.dto;

import lombok.Data;

@Data
public class LoginRequestData {
    private String username;
    private String password;
}
