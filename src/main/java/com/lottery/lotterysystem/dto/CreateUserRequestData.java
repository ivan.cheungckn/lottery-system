package com.lottery.lotterysystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class CreateUserRequestData {
    @NonNull
    private String username;
    @NonNull
    private String password;
    @NonNull
    private String email;
}
