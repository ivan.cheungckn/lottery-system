package com.lottery.lotterysystem.dto;

import lombok.Data;

@Data
public class EmailRequestData {
    private String fromEmail;
    private String toEmail;
    private String subject;
    private String body;
}
