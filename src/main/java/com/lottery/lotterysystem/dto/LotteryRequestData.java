package com.lottery.lotterysystem.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class LotteryRequestData {
    @NotNull
    private Boolean isActive;
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date startTime;
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date endTime;
    @NotNull
    private String name;
    @NotNull
    private String description;
    @NotNull
    private Long intervalSecond;
    @NotNull
    private String drawStrategyType;
    @NotNull
    private Long ticketPrice;
}
