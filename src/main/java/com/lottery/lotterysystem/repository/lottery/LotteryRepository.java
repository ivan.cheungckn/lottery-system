package com.lottery.lotterysystem.repository.lottery;

import com.lottery.lotterysystem.entity.LotteryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LotteryRepository extends JpaRepository<LotteryEntity, Long> {
}
