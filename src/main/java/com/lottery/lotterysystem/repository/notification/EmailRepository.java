package com.lottery.lotterysystem.repository.notification;

import com.lottery.lotterysystem.entity.EmailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<EmailEntity, Long> {
}
