package com.lottery.lotterysystem.repository.lotteryEvent;

import com.lottery.lotterysystem.entity.EventStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventStatusRepository extends JpaRepository<EventStatusEntity, Long> {
    EventStatusEntity findByName(String name);
}
