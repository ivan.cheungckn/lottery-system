package com.lottery.lotterysystem.repository.lotteryEvent;

import com.lottery.lotterysystem.entity.EventStatusEntity;
import com.lottery.lotterysystem.entity.LotteryEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface LotteryEventRepository extends JpaRepository<LotteryEventEntity, Long> {
    @Query(value = "SELECT le FROM LotteryEventEntity le WHERE le.startTime <= ?1 AND le.endTime >= ?1")
    List<LotteryEventEntity> findAllOnGoingEvents(Date currentTime);

    List<LotteryEventEntity> findAllByEventStatusEntity(EventStatusEntity eventStatusEntity);
}
