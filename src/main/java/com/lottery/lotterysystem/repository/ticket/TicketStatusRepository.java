package com.lottery.lotterysystem.repository.ticket;

import com.lottery.lotterysystem.entity.TicketStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketStatusRepository extends JpaRepository<TicketStatusEntity, Long> {
    TicketStatusEntity findByName(String name);
}
