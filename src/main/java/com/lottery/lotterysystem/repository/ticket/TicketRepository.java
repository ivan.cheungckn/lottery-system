package com.lottery.lotterysystem.repository.ticket;

import com.lottery.lotterysystem.entity.LotteryEventEntity;
import com.lottery.lotterysystem.entity.TicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TicketRepository extends JpaRepository<TicketEntity, Long> {
    @Query(value = "SELECT l.ticketPrice FROM LotteryEntity l JOIN LotteryEventEntity le ON l.id = le.lotteryEntity.id WHERE le.id = ?1")
    Long getTicketPriceByLotteryEventId(Long eventId);

    @Query(value = "SELECT t.id FROM TicketEntity t JOIN UserEntity u ON u.id = t.userEntity.id JOIN TicketStatusEntity ts ON ts.id = t.status.id WHERE u.username = ?1 AND t.lotteryEventEntity.id = ?2 AND ts.name = ?3 ")
    Long isUserBoughtTicketInLotteryEvent(String username, Long eventId, String ticketStatus);

    @Query(value = "SELECT t FROM TicketEntity t JOIN UserEntity u ON u.id = t.userEntity.id JOIN LotteryEventEntity le ON le.id = t.lotteryEventEntity.id WHERE u.username = ?1 AND le.id = ?2")
    TicketEntity findByUsernameAndEventId(String username, Long eventId);

    @Query(value = "SELECT t FROM LotteryEventEntity le JOIN TicketEntity t ON t.lotteryEventEntity.id = le.id WHERE t.emailEntity.id IS NULL AND le = ?1")
    List<TicketEntity> findAllNotYetSendEmailByEvent(LotteryEventEntity lotteryEventEntity);

    @Query(value = "SELECT t FROM TicketEntity t JOIN PaymentTransactionEntity pt ON pt.ticketEntity.id = t.id JOIN LotteryEventEntity le ON le.id = t.lotteryEventEntity.id JOIN PaymentStatusEntity ps ON ps.id = pt.paymentStatusEntity.id WHERE ps.name = 'complete' AND le.id = ?1")
    List<TicketEntity> countAllCompleteTransactionByEventId(Long eventId);

    List<TicketEntity> findAllByUserEntityUsername(String username);

    List<TicketEntity> findAllByLotteryEventEntity(LotteryEventEntity lotteryEventEntity);
}
