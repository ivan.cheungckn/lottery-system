package com.lottery.lotterysystem.repository.payment;

import com.lottery.lotterysystem.entity.PaymentStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentStatusRepository extends JpaRepository<PaymentStatusEntity, Long> {

    PaymentStatusEntity findByName(String name);
}
