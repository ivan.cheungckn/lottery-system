package com.lottery.lotterysystem.repository.payment;

import com.lottery.lotterysystem.entity.PaymentTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentTypeRepository extends JpaRepository<PaymentTypeEntity, Long> {
    PaymentTypeEntity findByName(String name);
}
