package com.lottery.lotterysystem.repository.payment;

import com.lottery.lotterysystem.entity.PaymentTransactionEntity;
import com.lottery.lotterysystem.entity.StripePaymentEntityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StripePaymentRepository extends JpaRepository<StripePaymentEntityEntity, Long> {
    StripePaymentEntityEntity findByPaymentTransactionEntity(PaymentTransactionEntity paymentTransactionEntity);

    StripePaymentEntityEntity findByPaymentIntent(String paymentIntent);

    @Query(value = "SELECT sp FROM StripePaymentEntityEntity sp JOIN PaymentTransactionEntity pt ON pt.id = sp.paymentTransactionEntity.id JOIN TicketEntity t ON t.id = pt.ticketEntity.id JOIN LotteryEventEntity le ON le.id = t.lotteryEventEntity.id WHERE le.id = ?1")
    List<StripePaymentEntityEntity> findAllByEventId(Long eventId);
}
