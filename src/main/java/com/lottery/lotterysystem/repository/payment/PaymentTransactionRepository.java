package com.lottery.lotterysystem.repository.payment;

import com.lottery.lotterysystem.entity.PaymentTransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PaymentTransactionRepository extends JpaRepository<PaymentTransactionEntity, Long> {
    @Query(value = "SELECT pt FROM PaymentTransactionEntity pt JOIN TicketEntity t ON pt.ticketEntity.id = t.id JOIN LotteryEventEntity le ON le.id = t.lotteryEventEntity.id JOIN UserEntity u ON u.id = t.userEntity.id JOIN PaymentTypeEntity ptype ON ptype.id = pt.paymentTypeEntity.id WHERE le.id = ?1 AND u.username = ?2 AND ptype.name = ?3")
    PaymentTransactionEntity findByEventIdAndUsernameAndPaymentType(Long eventId, String username, String paymentType);

    //
//    @Query(value = "SELECT count(pt.id) FROM payment_transaction pt JOIN ticket t ON t.id = pt.ticket_id JOIN lottery_event le ON le.id = t.lottery_event_id JOIN payment_status ps ON ps.id = pt.payment_status_id WHERE le.id = 5 AND (ps.name <> 'succeeded' OR ps.name <> 'canceled')")
    @Query(value = "SELECT count(pt.id) FROM PaymentTransactionEntity pt JOIN TicketEntity t ON t.id = pt.ticketEntity.id JOIN LotteryEventEntity le ON le.id = t.lotteryEventEntity.id JOIN PaymentStatusEntity ps ON ps.id = pt.paymentStatusEntity.id WHERE le.id = ?1 AND (ps.name <> 'complete' AND ps.name <> 'cancel')")
    Long findTotalNonCancelAndCompleteTransaction(Long eventId);
}
