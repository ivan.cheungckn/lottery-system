package com.lottery.lotterysystem.repository.drawStrategy;

import com.lottery.lotterysystem.entity.DrawStrategyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DrawStrategyRepository extends JpaRepository<DrawStrategyEntity, Long> {
    @Query(value = "SELECT ds.id FROM DrawStrategyEntity ds JOIN LotteryEntity l ON l.drawStrategyEntity.id = ds.id JOIN LotteryEventEntity le ON le.lotteryEntity.id = l.id WHERE le.id = ?1")
    DrawStrategyEntity findByEventId(Long eventId);

    DrawStrategyEntity findByType(String type);
}
