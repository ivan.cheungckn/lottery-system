package com.lottery.lotterysystem.security;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class bCryptPasswordEncoder extends BCryptPasswordEncoder {

}
