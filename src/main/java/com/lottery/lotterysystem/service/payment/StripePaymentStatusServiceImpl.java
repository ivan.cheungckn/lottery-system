package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.entity.PaymentStatusEntity;
import com.lottery.lotterysystem.repository.payment.PaymentStatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@AllArgsConstructor
@Transactional
public class StripePaymentStatusServiceImpl {
    private final PaymentStatusRepository paymentStatusRepository;

    public PaymentStatusEntity findByName(String name) {
        return paymentStatusRepository.findByName(name);
    }
}
