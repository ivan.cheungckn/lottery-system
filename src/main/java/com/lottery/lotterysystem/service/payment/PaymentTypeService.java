package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.entity.PaymentTypeEntity;

public interface PaymentTypeService {
    PaymentTypeEntity findByName(String name);
}
