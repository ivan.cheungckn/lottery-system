package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.entity.PaymentStatusEntity;

public interface StripePaymentStatusService {
    PaymentStatusEntity findByName(String name);
}
