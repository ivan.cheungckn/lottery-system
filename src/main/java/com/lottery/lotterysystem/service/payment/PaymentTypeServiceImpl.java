package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.entity.PaymentTypeEntity;
import com.lottery.lotterysystem.repository.payment.PaymentTypeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Transactional
public class PaymentTypeServiceImpl implements PaymentTypeService {
    private final PaymentTypeRepository paymentTypeRepository;

    @Override
    public PaymentTypeEntity findByName(String name) {
        return paymentTypeRepository.findByName(name);
    }
}
