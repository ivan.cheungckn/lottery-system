package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.entity.PaymentStatusEntity;
import com.lottery.lotterysystem.entity.PaymentTransactionEntity;
import com.lottery.lotterysystem.entity.PaymentTypeEntity;
import com.lottery.lotterysystem.entity.TicketEntity;

public interface PaymentTransactionService {
    PaymentTransactionEntity findByEventIdAndUsernameAndPaymentType(Long eventId, String username, String paymentType);

    PaymentTransactionEntity savePaymentTransactionEntity(TicketEntity ticketEntity, PaymentTypeEntity paymentTypeEntity, PaymentStatusEntity paymentStatusEntity);

    PaymentTransactionEntity changeToSuccessStatus(PaymentTransactionEntity paymentTransactionEntity);

    PaymentTransactionEntity changeToCancelStatus(PaymentTransactionEntity paymentTransactionEntity);

    void updatePendingPaymentBeforeDrawByEventId(Long eventId);

    Long findTotalNonCancelAndCompleteTransaction(Long eventId);
}