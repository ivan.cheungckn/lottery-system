package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.entity.PaymentTransactionEntity;
import com.lottery.lotterysystem.entity.TicketEntity;
import com.lottery.lotterysystem.exception.PaymentException;

public abstract class AbstractPaymentService {
    String type;

    public abstract PaymentTransactionEntity handlePayment(TicketEntity ticketEntity, Long eventId, String username) throws PaymentException;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
