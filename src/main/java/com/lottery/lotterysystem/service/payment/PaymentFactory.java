package com.lottery.lotterysystem.service.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component

public class PaymentFactory {
    @Autowired
    private List<AbstractPaymentService> abstractPaymentServiceList;

    private static final Map<String, AbstractPaymentService> paymentServiceMap = new HashMap<>();

    @PostConstruct
    public void initPaymentServiceMap() {
        for (AbstractPaymentService abstractPaymentService : abstractPaymentServiceList) {
            paymentServiceMap.put(abstractPaymentService.getType(), abstractPaymentService);
        }
    }

    public static AbstractPaymentService getPaymentService(String type) {
        AbstractPaymentService abstractPaymentService = paymentServiceMap.get(type);
        if (Objects.isNull(abstractPaymentService)) throw new RuntimeException("Unknown payment service type: " + type);
        return abstractPaymentService;
    }
}

