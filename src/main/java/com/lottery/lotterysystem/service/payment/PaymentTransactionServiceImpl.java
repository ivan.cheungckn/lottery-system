package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.entity.PaymentStatusEntity;
import com.lottery.lotterysystem.entity.PaymentTransactionEntity;
import com.lottery.lotterysystem.entity.PaymentTypeEntity;
import com.lottery.lotterysystem.entity.TicketEntity;
import com.lottery.lotterysystem.enums.PaymentStatus;
import com.lottery.lotterysystem.repository.payment.PaymentTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PaymentTransactionServiceImpl implements PaymentTransactionService {
    private PaymentTransactionRepository paymentTransactionRepository;
    private PaymentStatusService paymentStatusService;
    private StripePaymentService stripePaymentService;


    @Override
    public PaymentTransactionEntity findByEventIdAndUsernameAndPaymentType(Long eventId, String username, String paymentType) {
        return paymentTransactionRepository.findByEventIdAndUsernameAndPaymentType(eventId, username, paymentType);
    }

    public PaymentTransactionEntity savePaymentTransactionEntity(TicketEntity ticketEntity, PaymentTypeEntity paymentTypeEntity, PaymentStatusEntity paymentStatusEntity) {
        PaymentTransactionEntity paymentTransactionEntity = new PaymentTransactionEntity();
        paymentTransactionEntity.setPaymentTypeEntity(paymentTypeEntity);
        paymentTransactionEntity.setTicketEntity(ticketEntity);
        paymentTransactionEntity.setPaymentStatusEntity(paymentStatusEntity);
        return paymentTransactionRepository.save(paymentTransactionEntity);
    }

    @Override
    public PaymentTransactionEntity changeToSuccessStatus(PaymentTransactionEntity paymentTransactionEntity) {
        return changeToPaymentStatus(paymentTransactionEntity, PaymentStatus.COMPLETE);
    }

    @Override
    public PaymentTransactionEntity changeToCancelStatus(PaymentTransactionEntity paymentTransactionEntity) {
        return changeToPaymentStatus(paymentTransactionEntity, PaymentStatus.CANCEL);
    }

    @Override
    public void updatePendingPaymentBeforeDrawByEventId(Long eventId) {
        stripePaymentService.updatePendingPaymentBeforeDrawByEventId(eventId);
    }

    @Override
    public Long findTotalNonCancelAndCompleteTransaction(Long eventId) {
        return paymentTransactionRepository.findTotalNonCancelAndCompleteTransaction(eventId);
    }

    private PaymentTransactionEntity changeToPaymentStatus(PaymentTransactionEntity paymentTransactionEntity, PaymentStatus paymentStatus) {
        PaymentStatusEntity paymentStatusEntity = paymentStatusService.findByName(paymentStatus.toString());
        paymentTransactionEntity.setPaymentStatusEntity(paymentStatusEntity);
        return paymentTransactionRepository.save(paymentTransactionEntity);
    }

    @Autowired
    public void setPaymentTransactionRepository(PaymentTransactionRepository paymentTransactionRepository) {
        this.paymentTransactionRepository = paymentTransactionRepository;
    }

    @Autowired
    public void setPaymentStatusService(PaymentStatusService paymentStatusService) {
        this.paymentStatusService = paymentStatusService;
    }

    @Autowired
    public void setStripePaymentService(StripePaymentService stripePaymentService) {
        this.stripePaymentService = stripePaymentService;
    }
}
