package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.dto.StripeResponseData;
import com.lottery.lotterysystem.entity.PaymentTransactionEntity;
import com.lottery.lotterysystem.entity.StripePaymentEntityEntity;
import com.lottery.lotterysystem.entity.TicketEntity;
import com.lottery.lotterysystem.exception.InvalidPriceException;

public interface StripePaymentService {
    StripePaymentEntityEntity saveStripePaymentEntity(String clientSecret, PaymentTransactionEntity paymentTransactionEntity, String paymentIntent);

    PaymentTransactionEntity createPaymentIntentIfNeeded(TicketEntity ticketEntity, Long eventId, String username) throws InvalidPriceException;

    StripeResponseData getResponseData(PaymentTransactionEntity paymentTransactionEntity);

    boolean handlePaymentIntentCallback(String paymentIntentId);

    void updatePendingPaymentBeforeDrawByEventId(Long eventId);

    StripePaymentEntityEntity updateStripePaymentEntity(String clientSecret, String paymentIntent);
}
