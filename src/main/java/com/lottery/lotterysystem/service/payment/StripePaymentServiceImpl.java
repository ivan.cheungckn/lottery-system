package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.dto.StripeResponseData;
import com.lottery.lotterysystem.entity.*;
import com.lottery.lotterysystem.enums.PaymentStatus;
import com.lottery.lotterysystem.enums.PaymentType;
import com.lottery.lotterysystem.enums.StripePaymentStatus;
import com.lottery.lotterysystem.exception.InvalidPriceException;
import com.lottery.lotterysystem.exception.PaymentException;
import com.lottery.lotterysystem.repository.payment.StripePaymentRepository;
import com.lottery.lotterysystem.service.ticket.TicketService;
import com.stripe.Stripe;
import com.stripe.model.PaymentIntent;
import com.stripe.net.RequestOptions;
import com.stripe.param.PaymentIntentCreateParams;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class StripePaymentServiceImpl extends AbstractPaymentService implements StripePaymentService {
    final Logger log = LogManager.getLogger(StripePaymentServiceImpl.class);

    private StripePaymentRepository stripePaymentRepository;
    private PaymentTransactionService paymentTransactionService;
    private PaymentStatusService paymentStatusService;
    private PaymentTypeService paymentTypeService;
    private TicketService ticketService;
    @Value("${stripe.api.key}")
    private String apiKey;

    @Autowired
    public StripePaymentServiceImpl() {
        this.type = PaymentType.STRIPE.toString();
    }

    @PostConstruct
    private void setStripeApiKey() {
        Stripe.apiKey = this.apiKey;
    }

    @Override
    public PaymentTransactionEntity createPaymentIntentIfNeeded(TicketEntity ticketEntity, Long eventId, String username) throws InvalidPriceException {
        PaymentTransactionEntity paymentTransactionEntity = paymentTransactionService.findByEventIdAndUsernameAndPaymentType(eventId, username, type);
        PaymentIntent paymentIntent;
        PaymentTypeEntity paymentTypeEntity = paymentTypeService.findByName(type);
        PaymentStatusEntity paymentStatusEntity = paymentStatusService.findByName(PaymentStatus.PENDING.toString());
        StripePaymentEntityEntity stripePaymentEntity = stripePaymentRepository.findByPaymentTransactionEntity(paymentTransactionEntity);
        Long ticketPrice = ticketService.findTicketPriceByEventId(eventId);
        if (Objects.isNull(stripePaymentEntity)) {
            paymentIntent = createPaymentIntent(ticketPrice, eventId, username);
            if (Objects.nonNull(paymentIntent)) {
                paymentTransactionEntity = saveTransactionEntityAndStripePaymentEntity(ticketEntity, paymentTypeEntity, paymentStatusEntity, paymentIntent);
            }
        } else {
            paymentIntent = retrievePaymentIntent(stripePaymentEntity);
            if (!isPaymentIntentValid(paymentIntent)) {
                paymentIntent = createPaymentIntent(ticketPrice, eventId, username);
                updateStripePaymentEntity(paymentIntent.getClientSecret(), paymentIntent.getId());
            }
        }
        return paymentTransactionEntity;
    }

    private boolean isPaymentIntentValid(PaymentIntent paymentIntent) {
        return !StringUtils.equalsIgnoreCase(paymentIntent.getStatus(), StripePaymentStatus.CANCELED.toString());

    }

    private PaymentTransactionEntity saveTransactionEntityAndStripePaymentEntity(TicketEntity ticketEntity, PaymentTypeEntity paymentTypeEntity, PaymentStatusEntity paymentStatusEntity, PaymentIntent paymentIntent) {
        PaymentTransactionEntity paymentTransactionEntity = paymentTransactionService.savePaymentTransactionEntity(ticketEntity, paymentTypeEntity, paymentStatusEntity);
        saveStripePaymentEntity(paymentIntent.getClientSecret(), paymentTransactionEntity, paymentIntent.getId());
        return paymentTransactionEntity;
    }

    @Override
    public StripePaymentEntityEntity updateStripePaymentEntity(String clientSecret, String paymentIntent) {
        StripePaymentEntityEntity stripePaymentEntity = new StripePaymentEntityEntity();
        stripePaymentEntity.setClientSecret(clientSecret);
        stripePaymentEntity.setPaymentIntent(paymentIntent);
        return stripePaymentRepository.save(stripePaymentEntity);
    }

    @Override
    public StripePaymentEntityEntity saveStripePaymentEntity(String clientSecret, PaymentTransactionEntity paymentTransactionEntity, String paymentIntent) {
        StripePaymentEntityEntity stripePaymentEntity = new StripePaymentEntityEntity();
        stripePaymentEntity.setPaymentTransactionEntity(paymentTransactionEntity);
        stripePaymentEntity.setClientSecret(clientSecret);
        stripePaymentEntity.setPaymentIntent(paymentIntent);
        return stripePaymentRepository.save(stripePaymentEntity);
    }

    private PaymentIntent createPaymentIntent(Long ticketPrice, Long eventId, String username) {
        try {
            PaymentIntentCreateParams params =
                    PaymentIntentCreateParams.builder()
                            .setAmount(ticketPrice)
                            .setCurrency("usd")
                            .setAutomaticPaymentMethods(
                                    PaymentIntentCreateParams.AutomaticPaymentMethods
                                            .builder()
                                            .setEnabled(true)
                                            .build()
                            )
                            .build();
            String idempotencyKey = String.format("stripe-%s-%s", username, eventId);
            RequestOptions requestOptions = new RequestOptions.RequestOptionsBuilder().setIdempotencyKey(idempotencyKey).build();
            // Create a PaymentIntent with the order amount and currency

            return PaymentIntent.create(params, requestOptions);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public PaymentTransactionEntity handlePayment(TicketEntity ticketEntity, Long eventId, String username) throws PaymentException {
        PaymentTransactionEntity paymentTransactionEntity;
        try {
            paymentTransactionEntity = createPaymentIntentIfNeeded(ticketEntity, eventId, username);

        } catch (Exception e) {
            log.error("Error when creating or getting payment intent");
            log.error(e.getMessage(), e);
            throw new PaymentException("Error when creating or getting payment intent");
        }
        return paymentTransactionEntity;
    }

    //    private void updatePaymentIntent
    @Override
    public StripeResponseData getResponseData(PaymentTransactionEntity paymentTransactionEntity) {
        StripePaymentEntityEntity stripePaymentEntity = stripePaymentRepository.findByPaymentTransactionEntity(paymentTransactionEntity);
        StripeResponseData stripeResponseData = new StripeResponseData();
        stripeResponseData.setClientSecret(stripePaymentEntity.getClientSecret());
        stripeResponseData.setSuccess(true);
        return stripeResponseData;
    }

    @Override
    public boolean handlePaymentIntentCallback(String paymentIntentId) {
        try {
            PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
            if (Objects.nonNull(paymentIntent)) {
                StripePaymentEntityEntity stripePaymentEntity = stripePaymentRepository.findByPaymentIntent(paymentIntentId);
                PaymentTransactionEntity paymentTransactionEntity = stripePaymentEntity.getPaymentTransactionEntity();
//                stripePaymentRepository.findByPaymentIntent(paymentIntentId);
                handlePaymentStatus(paymentIntent, paymentTransactionEntity);
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }

    private void handlePaymentStatus(PaymentIntent paymentIntent, PaymentTransactionEntity paymentTransactionEntity) {
        String status = paymentIntent.getStatus();
        if (StringUtils.equalsIgnoreCase(status, StripePaymentStatus.SUCCEEDED.toString())) {
            paymentTransactionService.changeToSuccessStatus(paymentTransactionEntity);
            ticketService.changeToPaidStatus(paymentTransactionEntity.getTicketEntity());
        } else if (StringUtils.equalsIgnoreCase(status, StripePaymentStatus.CANCELED.toString())) {
            paymentTransactionService.changeToCancelStatus(paymentTransactionEntity);
        }
    }

    @Override
    public void updatePendingPaymentBeforeDrawByEventId(Long eventId) {
        //Only a PaymentIntent with one of the following statuses may be canceled:
        // requires_payment_method, requires_capture, requires_confirmation, requires_action, processing.
        List<StripePaymentEntityEntity> stripePaymentEntityList = stripePaymentRepository.findAllByEventId(eventId);
        stripePaymentEntityList.forEach(entity -> {
            String status = entity.getPaymentTransactionEntity().getPaymentStatusEntity().getName();
            if (!StringUtils.equalsIgnoreCase(status, PaymentStatus.COMPLETE.toString())) {
                // only check for those payment transaction not complete
                updatePaymentIntentAndTicketStatus(entity, entity.getPaymentTransactionEntity());
            }
        });
    }

    private void updatePaymentIntentAndTicketStatus(StripePaymentEntityEntity stripePaymentEntity, PaymentTransactionEntity paymentTransactionEntity) {
        PaymentIntent paymentIntent = retrievePaymentIntent(stripePaymentEntity);
        if (Objects.nonNull(paymentIntent)) {
            if (StringUtils.equalsIgnoreCase(paymentIntent.getStatus(), StripePaymentStatus.SUCCEEDED.toString())) {
                // may be some are not calling /stripe/callback to change to complete status
                paymentTransactionService.changeToSuccessStatus(stripePaymentEntity.getPaymentTransactionEntity());
                ticketService.changeToPaidStatus(paymentTransactionEntity.getTicketEntity());
            } else {
                cancelPaymentIntent(stripePaymentEntity);
            }
        }
    }

    private void cancelPaymentIntent(StripePaymentEntityEntity stripePaymentEntity) {
        PaymentIntent paymentIntent = retrievePaymentIntent(stripePaymentEntity);
        if (Objects.nonNull(paymentIntent) && !StringUtils.equalsIgnoreCase(paymentIntent.getStatus(), PaymentStatus.CANCEL.toString())) {
            try {
                paymentIntent.cancel();
                paymentTransactionService.changeToCancelStatus(stripePaymentEntity.getPaymentTransactionEntity());
            } catch (Exception e) {
                log.error("Fail to cancel payment intent", e);
            }
        }
    }

    private PaymentIntent retrievePaymentIntent(StripePaymentEntityEntity stripePaymentEntity) {
        try {
            return PaymentIntent.retrieve(stripePaymentEntity.getPaymentIntent());
        } catch (Exception e) {
            log.error("Fail to retrieve payment intent", e);
            return null;
        }
    }

    @Autowired
    public void setStripePaymentRepository(StripePaymentRepository stripePaymentRepository) {
        this.stripePaymentRepository = stripePaymentRepository;
    }

    @Autowired
    public void setPaymentTransactionService(PaymentTransactionService paymentTransactionService) {
        this.paymentTransactionService = paymentTransactionService;
    }

    @Autowired
    public void setPaymentStatusService(PaymentStatusService paymentStatusService) {
        this.paymentStatusService = paymentStatusService;
    }

    @Autowired
    public void setPaymentTypeService(PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }

    @Autowired
    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }
}
