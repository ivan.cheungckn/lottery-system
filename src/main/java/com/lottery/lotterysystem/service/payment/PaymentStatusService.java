package com.lottery.lotterysystem.service.payment;

import com.lottery.lotterysystem.entity.PaymentStatusEntity;

public interface PaymentStatusService {
    PaymentStatusEntity findByName(String name);
}
