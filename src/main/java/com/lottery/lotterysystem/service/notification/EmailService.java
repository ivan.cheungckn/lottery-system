package com.lottery.lotterysystem.service.notification;

import com.lottery.lotterysystem.dto.EmailRequestData;
import com.lottery.lotterysystem.entity.EmailEntity;

import javax.mail.MessagingException;

public interface EmailService {
    EmailEntity sendEmail(EmailRequestData emailRequestData) throws MessagingException;

    EmailEntity saveEmail(EmailRequestData emailRequestData);
}
