package com.lottery.lotterysystem.service.notification;

import com.lottery.lotterysystem.dto.EmailRequestData;
import com.lottery.lotterysystem.entity.EmailEntity;
import com.lottery.lotterysystem.repository.notification.EmailRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Service
@Transactional
public class EmailServiceImpl implements EmailService {
    final Logger log = LogManager.getLogger(EmailServiceImpl.class);
    private JavaMailSender mailSender;
    private MailProperties mailProperties;
    private EmailRepository emailRepository;

    @Value("${spring.mail.username}")
    private String fromEmail;

    @Autowired
    public EmailServiceImpl(JavaMailSender mailSender, MailProperties mailProperties, EmailRepository emailRepository) {
        this.mailSender = mailSender;
        this.mailProperties = mailProperties;
        this.emailRepository = emailRepository;
    }

    @Override
    public EmailEntity sendEmail(EmailRequestData emailRequestData) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, StandardCharsets.UTF_8.toString());
        messageHelper.setSubject(emailRequestData.getSubject());
        messageHelper.setText(emailRequestData.getBody(), true);
        messageHelper.setFrom(Objects.isNull(emailRequestData.getFromEmail()) ? fromEmail : emailRequestData.getFromEmail());
        messageHelper.setTo(emailRequestData.getToEmail());
        mailSender.send(message);
        return saveEmail(emailRequestData);
    }

    @Override
    public EmailEntity saveEmail(EmailRequestData emailRequestData) {
        EmailEntity emailEntity = new EmailEntity();
        emailEntity.setToEmail(emailRequestData.getToEmail());
        emailEntity.setSubject(emailRequestData.getSubject());
        emailEntity.setContent(emailRequestData.getBody());
        return emailRepository.save(emailEntity);

    }
}
