package com.lottery.lotterysystem.service.ticket;

import com.lottery.lotterysystem.entity.TicketStatusEntity;

public interface TicketStatusService {
    TicketStatusEntity findByName(String name);
}
