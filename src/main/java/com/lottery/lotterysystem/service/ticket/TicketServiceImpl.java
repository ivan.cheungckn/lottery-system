package com.lottery.lotterysystem.service.ticket;

import com.lottery.lotterysystem.entity.*;
import com.lottery.lotterysystem.enums.TicketStatus;
import com.lottery.lotterysystem.exception.EventTicketNotSellException;
import com.lottery.lotterysystem.exception.InvalidPriceException;
import com.lottery.lotterysystem.exception.NotAllowedException;
import com.lottery.lotterysystem.exception.PaymentException;
import com.lottery.lotterysystem.repository.ticket.TicketRepository;
import com.lottery.lotterysystem.service.lotteryEvent.LotteryEventService;
import com.lottery.lotterysystem.service.payment.AbstractPaymentService;
import com.lottery.lotterysystem.service.payment.PaymentFactory;
import com.lottery.lotterysystem.service.user.UserService;
import com.mchange.util.AlreadyExistsException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {
    final Logger log = LogManager.getLogger(TicketServiceImpl.class);

    private TicketRepository ticketRepository;
    private LotteryEventService lotteryEventService;
    private UserService userService;
    private TicketStatusService ticketStatusService;

    public TicketServiceImpl() {
    }

    private boolean isUserBoughtTicketInLotteryEvent(String username, Long eventId) {
        Long ticketId = ticketRepository.isUserBoughtTicketInLotteryEvent(username, eventId, TicketStatus.PAID.toString());
        return Objects.nonNull(ticketId) && ticketId > 0;
    }

    private boolean isAllowedToBuyTicket(String username, Long eventId, TicketEntity ticketEntity) throws AlreadyExistsException, EventTicketNotSellException {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(eventId.toString())) return false;
        // if user already bought ticket / event is not yet start or closed will reject
        boolean isUserBoughtInLotteryEvent = Objects.nonNull(ticketEntity) &&
                StringUtils.equalsIgnoreCase(ticketEntity.getStatus().getName(), TicketStatus.PAID.toString());
        if (isUserBoughtInLotteryEvent) {
            log.error("User {} already bought ticket in the event {}", username, eventId);
            throw new AlreadyExistsException("User already bought ticket in the event");
        }
        boolean isEventOpenToSell = isEventOpenToSellTicket(eventId);
        if (BooleanUtils.isFalse(isEventOpenToSell)) {
            log.error("Event {} is not open to sell ticket", eventId);
            throw new EventTicketNotSellException("Event is not open to sell ticket");
        }
        return true;
    }

    public PaymentTransactionEntity buyTicket(Long eventId, String paymentType) throws AlreadyExistsException, EventTicketNotSellException, PaymentException, NotAllowedException {
        // check user is exist
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (StringUtils.isBlank(username)) {
            log.error("User is not found in securityContextHolder");
            throw new UsernameNotFoundException("User not found");
        }
        TicketEntity ticketEntity = ticketRepository.findByUsernameAndEventId(username, eventId);
        boolean isAllowedToBuyTicket = isAllowedToBuyTicket(username, eventId, ticketEntity);
        if (BooleanUtils.isFalse(isAllowedToBuyTicket)) {
            throw new NotAllowedException("Not allow to buy ticket");
        }
        ;

        if (Objects.isNull(ticketEntity)) {
            //creat ticket
            log.info("Create ticket entity for user {}", username);
            ticketEntity = createTicket(username, eventId);
        }
        // create payment info
        AbstractPaymentService paymentService = PaymentFactory.getPaymentService(paymentType);
        return paymentService.handlePayment(ticketEntity, eventId, username);
    }

    public TicketEntity saveTicket(TicketEntity ticketEntity) {
        return ticketRepository.save(ticketEntity);
    }

    public List<TicketEntity> findAllNotYetSendEmail(LotteryEventEntity lotteryEventEntity) {
        return ticketRepository.findAllNotYetSendEmailByEvent(lotteryEventEntity);
    }

    @Override
    public List<TicketEntity> countAllCompleteTransactionByEventId(Long eventId) {
        return ticketRepository.countAllCompleteTransactionByEventId(eventId);
    }

    public TicketEntity createTicket(String username, Long eventId) {
        if (StringUtils.isBlank(username)) throw new UsernameNotFoundException("Username not found");
        Optional<LotteryEventEntity> lotteryEventEntity = lotteryEventService.findById(eventId);
        if (lotteryEventEntity.isPresent()) {
            TicketStatusEntity ticketStatusEntity = ticketStatusService.findByName(TicketStatus.PENDING.toString());
            if (Objects.isNull(ticketStatusEntity)) throw new EntityNotFoundException("Ticket status not found");
            TicketEntity ticketEntity = new TicketEntity();
            UserEntity userEntity = userService.getUser(username);
            if (Objects.isNull(userEntity)) throw new EntityNotFoundException("User not found");
            ticketEntity.setLotteryEventEntity(lotteryEventEntity.get());
            ticketEntity.setStatus(ticketStatusEntity);
            ticketEntity.setUserEntity(userEntity);
            return ticketRepository.save(ticketEntity);
        } else {
            log.error("Event {} not found", eventId);
            throw new EntityNotFoundException(String.format("Event %s not found", eventId));
        }
    }

    public Long findTicketPriceByEventId(Long eventId) throws InvalidPriceException {
        Long ticketPrice = ticketRepository.getTicketPriceByLotteryEventId(eventId);
        if (Objects.isNull(ticketPrice) || ticketPrice <= 0) {
            log.error("Ticket price is null or smaller or equal to zero");
            throw new InvalidPriceException();
        }
        return ticketPrice;
    }

    @Override
    public void removeAll(List<TicketEntity> ticketEntities) {
        ticketRepository.deleteAll(ticketEntities);
    }

    @Override
    public TicketEntity changeToPaidStatus(TicketEntity ticketEntity) {
        TicketStatusEntity ticketStatusEntity = ticketStatusService.findByName(TicketStatus.PAID.toString());
        ticketEntity.setStatus(ticketStatusEntity);
        return ticketRepository.save(ticketEntity);
    }

    @Override
    public List<TicketEntity> findAllMyTickets() {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (StringUtils.isBlank(username)) {
            log.error("User is not found in securityContextHolder");
            throw new UsernameNotFoundException("User not found");
        }
        return ticketRepository.findAllByUserEntityUsername(username);
    }

    @Override
    public List<TicketEntity> findAllByLotteryEventEntity(LotteryEventEntity lotteryEventEntity) {
        return ticketRepository.findAllByLotteryEventEntity(lotteryEventEntity);
    }

    @Override
    public List<TicketEntity> saveAll(List<TicketEntity> ticketEntities) {
        return ticketRepository.saveAll(ticketEntities);
    }

    private boolean isEventOpenToSellTicket(Long eventId) {
        Optional<LotteryEventEntity> lotteryEventEntityOptional = lotteryEventService.findById(eventId);

        if (lotteryEventEntityOptional.isPresent()) {
            LotteryEventEntity lotteryEventEntity = lotteryEventEntityOptional.get();
            Date currentDate = new Date();
            // if currentDate smaller than startTime return false
            if (currentDate.compareTo(lotteryEventEntity.getStartTime()) < 0) return false;
            // if currentDate greater or equal to endTime return false
            if (currentDate.compareTo(lotteryEventEntity.getEndTime()) >= 0) return false;
            return true;
        } else {
            return false;
        }
    }

    @Autowired
    public void setTicketRepository(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Autowired
    public void setLotteryEventService(LotteryEventService lotteryEventService) {
        this.lotteryEventService = lotteryEventService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTicketStatusService(TicketStatusService ticketStatusService) {
        this.ticketStatusService = ticketStatusService;
    }
}
