package com.lottery.lotterysystem.service.ticket;

import com.lottery.lotterysystem.entity.TicketStatusEntity;
import com.lottery.lotterysystem.repository.ticket.TicketStatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class TicketStatusServiceImpl implements TicketStatusService {
    private final TicketStatusRepository ticketStatusRepository;

    @Override
    @Transactional
    public TicketStatusEntity findByName(String name) {
        return ticketStatusRepository.findByName(name);
    }
}
