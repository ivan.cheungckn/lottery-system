package com.lottery.lotterysystem.service.ticket;

import com.lottery.lotterysystem.entity.LotteryEventEntity;
import com.lottery.lotterysystem.entity.PaymentTransactionEntity;
import com.lottery.lotterysystem.entity.TicketEntity;
import com.lottery.lotterysystem.exception.EventTicketNotSellException;
import com.lottery.lotterysystem.exception.InvalidPriceException;
import com.lottery.lotterysystem.exception.NotAllowedException;
import com.lottery.lotterysystem.exception.PaymentException;
import com.mchange.util.AlreadyExistsException;

import java.util.List;

public interface TicketService {
    TicketEntity createTicket(String username, Long eventId);

    Long findTicketPriceByEventId(Long eventId) throws InvalidPriceException;

    PaymentTransactionEntity buyTicket(Long eventId, String paymentType) throws AlreadyExistsException, EventTicketNotSellException, PaymentException, NotAllowedException;

    TicketEntity saveTicket(TicketEntity ticketEntity);

    List<TicketEntity> findAllNotYetSendEmail(LotteryEventEntity lotteryEventEntity);

    List<TicketEntity> countAllCompleteTransactionByEventId(Long eventId);

    void removeAll(List<TicketEntity> ticketEntities);

    TicketEntity changeToPaidStatus(TicketEntity ticketEntity);

    List<TicketEntity> findAllMyTickets();

    List<TicketEntity> findAllByLotteryEventEntity(LotteryEventEntity lotteryEventEntity);

    List<TicketEntity> saveAll(List<TicketEntity> ticketEntities);
}
