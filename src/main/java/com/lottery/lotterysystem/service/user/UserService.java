package com.lottery.lotterysystem.service.user;

import com.lottery.lotterysystem.dto.CreateUserRequestData;
import com.lottery.lotterysystem.entity.RoleEntity;
import com.lottery.lotterysystem.entity.UserEntity;

import java.util.List;

public interface UserService {
    UserEntity saveUser(UserEntity user);

    RoleEntity saveRole(RoleEntity roleEntity);

    void addRoleToUser(String username, String roleName);

    UserEntity getUser(String username);

    List<UserEntity> getUsers();

    UserEntity createUser(CreateUserRequestData createUserRequestData);
}
