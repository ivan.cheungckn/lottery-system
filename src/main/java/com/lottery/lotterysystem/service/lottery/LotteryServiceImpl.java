package com.lottery.lotterysystem.service.lottery;

import com.lottery.lotterysystem.dto.LotteryRequestData;
import com.lottery.lotterysystem.entity.DrawStrategyEntity;
import com.lottery.lotterysystem.entity.LotteryEntity;
import com.lottery.lotterysystem.exception.DrawStrategyNotFound;
import com.lottery.lotterysystem.repository.lottery.LotteryRepository;
import com.lottery.lotterysystem.service.drawStrategy.DrawStrategyService;
import com.lottery.lotterysystem.service.lotteryEvent.LotteryEventService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Transactional
public class LotteryServiceImpl implements LotteryService {
    final Logger log = LogManager.getLogger(LotteryServiceImpl.class);
    private final LotteryEventService lotteryEventService;
    private final DrawStrategyService drawStrategyService;
    private final LotteryRepository lotteryRepository;

    @Override
    public LotteryEntity createLottery(LotteryRequestData lotteryRequestData) throws DrawStrategyNotFound {
        DrawStrategyEntity drawStrategy = drawStrategyService.findByType(lotteryRequestData.getDrawStrategyType());
        if (Objects.isNull(drawStrategy))
            throw new DrawStrategyNotFound(String.format("Invalid type of draw strategy", lotteryRequestData.getDrawStrategyType()));
        LotteryEntity lotteryEntity = new LotteryEntity();
        lotteryEntity.setDrawStrategyEntity(drawStrategy);
        lotteryEntity.setIsActive(lotteryRequestData.getIsActive());
        lotteryEntity.setEndTime(lotteryRequestData.getEndTime());
        lotteryEntity.setStartTime(lotteryRequestData.getStartTime());
        lotteryEntity.setIntervalSecond(lotteryRequestData.getIntervalSecond());
        lotteryEntity.setDescription(lotteryRequestData.getDescription());
        lotteryEntity.setName(lotteryRequestData.getName());
        lotteryEntity.setTicketPrice(lotteryRequestData.getTicketPrice());
        return lotteryRepository.save(lotteryEntity);
    }

    public LotteryEntity createLotteryAndLotteryEvent(LotteryRequestData lotteryRequestData) throws DrawStrategyNotFound {
        LotteryEntity lotteryEntity = createLottery(lotteryRequestData);
        lotteryEventService.createLotteryEvent(lotteryEntity);
        return lotteryEntity;
    }
    //todo a queue -> cronjob put job into queue, 要save落db 做record, 因為同一個job會重復放入去
}
