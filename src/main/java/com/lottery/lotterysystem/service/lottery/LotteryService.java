package com.lottery.lotterysystem.service.lottery;

import com.lottery.lotterysystem.dto.LotteryRequestData;
import com.lottery.lotterysystem.entity.LotteryEntity;
import com.lottery.lotterysystem.exception.DrawStrategyNotFound;

public interface LotteryService {
    //    void drawWinner(Long eventId);
    LotteryEntity createLottery(LotteryRequestData lotteryRequestData) throws DrawStrategyNotFound;

    LotteryEntity createLotteryAndLotteryEvent(LotteryRequestData lotteryRequestData) throws DrawStrategyNotFound;
}
