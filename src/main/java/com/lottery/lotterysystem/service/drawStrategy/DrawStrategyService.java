package com.lottery.lotterysystem.service.drawStrategy;

import com.lottery.lotterysystem.entity.DrawStrategyEntity;
import com.lottery.lotterysystem.entity.LotteryEventEntity;

import java.util.Optional;

public interface DrawStrategyService {
    Optional<DrawStrategyEntity> findByEventId(Long eventId);

    DrawStrategyEntity findByType(String type);

    boolean draw(LotteryEventEntity lotteryEventEntity);
}
