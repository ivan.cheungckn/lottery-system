package com.lottery.lotterysystem.service.drawStrategy;

import com.lottery.lotterysystem.entity.DrawStrategyEntity;
import com.lottery.lotterysystem.entity.LotteryEventEntity;
import com.lottery.lotterysystem.enums.EventStatus;
import com.lottery.lotterysystem.repository.drawStrategy.DrawStrategyRepository;
import com.lottery.lotterysystem.service.lotteryEvent.EventStatusService;
import com.lottery.lotterysystem.service.lotteryEvent.LotteryEventService;
import com.lottery.lotterysystem.service.payment.PaymentTransactionService;
import com.lottery.lotterysystem.util.DrawStrategy.AbstractDrawStrategy;
import com.lottery.lotterysystem.util.DrawStrategy.DrawStrategyFactory;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional
public class DrawStrategyServiceImpl implements DrawStrategyService {
    final Logger log = LogManager.getLogger(DrawStrategyServiceImpl.class);
    private DrawStrategyRepository drawStrategyRepository;
    private PaymentTransactionService paymentTransactionService;
    private EventStatusService eventStatusService;
    private LotteryEventService lotteryEventService;

    @Override
    public Optional<DrawStrategyEntity> findByEventId(Long eventId) {
        return Optional.empty();
    }

    @Override
    public DrawStrategyEntity findByType(String type) {
        return drawStrategyRepository.findByType(type);
    }

    public boolean draw(LotteryEventEntity lotteryEventEntity) {
        if (isValidToDraw(lotteryEventEntity)) {
            String drawStrategyType = lotteryEventEntity.getLotteryEntity().getDrawStrategyEntity().getType();
            AbstractDrawStrategy drawStrategy = DrawStrategyFactory.getDrawStrategy(drawStrategyType);
            drawStrategy.drawWinner(lotteryEventEntity);
            lotteryEventEntity.setEventStatusEntity(eventStatusService.findByName(EventStatus.DRAWN.toString()));
            lotteryEventService.saveLotteryEvent(lotteryEventEntity);
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidToDraw(LotteryEventEntity lotteryEventEntity) {
        Date currentDate = new Date();
        //endtime > current -> false
        if (currentDate.compareTo(lotteryEventEntity.getEndTime()) <= 0) return false;
        if (!StringUtils.equalsIgnoreCase(lotteryEventEntity.getEventStatusEntity().getName(), EventStatus.OPEN.toString()))
            return false;
        Long totalNonCancelAndNonCompleteTransaction = paymentTransactionService.findTotalNonCancelAndCompleteTransaction(lotteryEventEntity.getId());
        if (Objects.nonNull(totalNonCancelAndNonCompleteTransaction) && totalNonCancelAndNonCompleteTransaction > 0)
            return false;
        return true;
    }
}
