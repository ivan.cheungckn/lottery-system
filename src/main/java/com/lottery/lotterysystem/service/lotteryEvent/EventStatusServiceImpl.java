package com.lottery.lotterysystem.service.lotteryEvent;

import com.lottery.lotterysystem.entity.EventStatusEntity;
import com.lottery.lotterysystem.repository.lotteryEvent.EventStatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EventStatusServiceImpl implements EventStatusService {
    private final EventStatusRepository eventStatusRepository;

    public EventStatusEntity findByName(String name) {
        return eventStatusRepository.findByName(name);
    }
}
