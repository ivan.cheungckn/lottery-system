package com.lottery.lotterysystem.service.lotteryEvent;

import com.lottery.lotterysystem.entity.EventStatusEntity;
import com.lottery.lotterysystem.entity.LotteryEntity;
import com.lottery.lotterysystem.entity.LotteryEventEntity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface LotteryEventService {
    Optional<LotteryEventEntity> findById(Long id);

    List<LotteryEventEntity> getOnGoingLotteryEvents();

    LotteryEventEntity createLotteryEvent(LotteryEntity lotteryEntity);

    List<LotteryEventEntity> findAllByStatus(EventStatusEntity eventStatusEntity);

    LotteryEventEntity saveLotteryEvent(LotteryEventEntity lotteryEventEntity);

    boolean createScheduleLotteryEvent(LotteryEventEntity lotteryEventEntity, String jobDetailGroupId, String triggerGroupId);

    boolean updateScheduleLotteryEvent(LotteryEventEntity lotteryEventEntity, String triggerGroupId);

    boolean updateScheduleLotteryEvent(LotteryEventEntity lotteryEventEntity, String triggerGroupId, Date startAt);

    boolean completeLotteryEventAndDiscardTickets(LotteryEventEntity lotteryEventEntity);
}
