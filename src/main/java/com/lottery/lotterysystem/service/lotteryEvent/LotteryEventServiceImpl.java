package com.lottery.lotterysystem.service.lotteryEvent;

import com.lottery.lotterysystem.entity.EventStatusEntity;
import com.lottery.lotterysystem.entity.LotteryEntity;
import com.lottery.lotterysystem.entity.LotteryEventEntity;
import com.lottery.lotterysystem.entity.TicketEntity;
import com.lottery.lotterysystem.enums.EventStatus;
import com.lottery.lotterysystem.quartz.job.DrawJob;
import com.lottery.lotterysystem.repository.lotteryEvent.LotteryEventRepository;
import com.lottery.lotterysystem.service.ticket.TicketService;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class LotteryEventServiceImpl implements LotteryEventService {
    final Logger log = LogManager.getLogger(LotteryEventServiceImpl.class);
    private LotteryEventRepository lotteryEventRepository;
    private EventStatusService eventStatusService;
    private TicketService ticketService;
    private Scheduler scheduler;

    public LotteryEventServiceImpl() {
    }

    @Override
    public Optional<LotteryEventEntity> findById(Long id) {
        return lotteryEventRepository.findById(id);
    }

    @Override
    @Cacheable(value = "ticket", key = "'ongoingEvents'")
    public List<LotteryEventEntity> getOnGoingLotteryEvents() {
        return lotteryEventRepository.findAllOnGoingEvents(new Date());
    }

    @Override
    @CacheEvict(value = "ticket", key = "'ongoingEvents'")
    public LotteryEventEntity createLotteryEvent(LotteryEntity lotteryEntity) {
        if (isValidToCreateLotteryEvent(lotteryEntity)) {
            LotteryEventEntity lotteryEventEntity = new LotteryEventEntity();
            Date currentDate = new Date();
            Date endDate = new Date(currentDate.getTime() + lotteryEntity.getIntervalSecond() * 1000);
            EventStatusEntity eventStatusEntity = eventStatusService.findByName(EventStatus.OPEN.toString());
            if (Objects.isNull(eventStatusEntity))
                throw new EntityNotFoundException("Event Status is not found");
            lotteryEventEntity.setLotteryEntity(lotteryEntity);
            lotteryEventEntity.setEventStatusEntity(eventStatusEntity);
            lotteryEventEntity.setEndTime(endDate);
            lotteryEventEntity.setStartTime(currentDate);
            return lotteryEventRepository.save(lotteryEventEntity);
        }
        return null;
    }

    @Override
    public List<LotteryEventEntity> findAllByStatus(EventStatusEntity eventStatusEntity) {
        return lotteryEventRepository.findAllByEventStatusEntity(eventStatusEntity);
    }

    @Override
    public LotteryEventEntity saveLotteryEvent(LotteryEventEntity lotteryEventEntity) {
        return lotteryEventRepository.save(lotteryEventEntity);
    }

    private boolean isValidToCreateLotteryEvent(LotteryEntity lotteryEntity) {
        if (BooleanUtils.isFalse(lotteryEntity.getIsActive())) return false;
        Date currentDate = new Date();
        // if currentDate larger or equal to endTime return false
        if (currentDate.compareTo(lotteryEntity.getEndTime()) > 0) return false;
        if (Objects.nonNull(lotteryEntity.getIntervalSecond()) && lotteryEntity.getIntervalSecond() <= 0) return false;
        return true;
    }

    @Override
    public boolean updateScheduleLotteryEvent(LotteryEventEntity lotteryEventEntity, String triggerGroupId) {
        return updateScheduleLotteryEvent(lotteryEventEntity, triggerGroupId, getCronJobStartAt(lotteryEventEntity));
    }

    @Override
    public boolean updateScheduleLotteryEvent(LotteryEventEntity lotteryEventEntity, String triggerGroupId, Date startAt) {
        try {
            TriggerKey triggerKey = new TriggerKey(lotteryEventEntity.getId().toString(), triggerGroupId);
            Trigger trigger = scheduler.getTrigger(triggerKey);
            TriggerBuilder triggerBuilder = trigger.getTriggerBuilder();
            Trigger newTrigger = triggerBuilder.startAt(startAt).build();
            scheduler.rescheduleJob(triggerKey, newTrigger);
            return true;
        } catch (Exception e) {
            log.error("Fail to update schedule of cronjob for event id: {}", lotteryEventEntity.getId());
            return false;
        }
    }

    @Override
    public boolean completeLotteryEventAndDiscardTickets(LotteryEventEntity lotteryEventEntity) {
        EventStatusEntity eventStatusEntity = eventStatusService.findByName(EventStatus.COMPLETE.toString());
        List<TicketEntity> ticketEntities = ticketService.findAllByLotteryEventEntity(lotteryEventEntity);
        lotteryEventEntity.setEventStatusEntity(eventStatusEntity);
        lotteryEventRepository.save(lotteryEventEntity);
//        ticketEntities.forEach(ticketEntity -> {
//            ticketEntity.setPaymentTransactionEntity(null);
//        });
//        ticketService.saveAll(ticketEntities);
        ticketService.removeAll(ticketEntities);
        return true;
    }

    private Date getCronJobStartAt(LotteryEventEntity lotteryEventEntity) {
        return new Date(lotteryEventEntity.getEndTime().getTime());
    }

    public boolean createScheduleLotteryEvent(LotteryEventEntity lotteryEventEntity, String jobDetailGroupId, String triggerGroupId) {
        try {
            JobDetail jobDetail = buildJobDetail(lotteryEventEntity, jobDetailGroupId);
            Trigger trigger = buildTrigger(jobDetail, getCronJobStartAt(lotteryEventEntity), triggerGroupId);
            scheduler.scheduleJob(jobDetail, trigger);
            return true;
        } catch (Exception e) {
            log.error("Fail to schedule cronjob for event id: {}", lotteryEventEntity.getId(), e);
            return false;
        }
    }

    private JobDetail buildJobDetail(LotteryEventEntity lotteryEventEntity, String jobDetailGroupId) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("eventId", lotteryEventEntity.getId());
        return JobBuilder.newJob(DrawJob.class)
                .withIdentity(lotteryEventEntity.getId().toString(), jobDetailGroupId)
                .withDescription(String.format("Drawing winner for event id: %s", lotteryEventEntity.getId()))
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    private Trigger buildTrigger(JobDetail jobDetail, Date startAt, String triggerGroupId) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), triggerGroupId)
                .withDescription(String.format("Drawing winner for event id: %s", jobDetail.getJobDataMap().get("eventId")))
                .startAt(startAt)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }

    @Autowired
    public void setLotteryEventRepository(LotteryEventRepository lotteryEventRepository) {
        this.lotteryEventRepository = lotteryEventRepository;
    }

    @Autowired
    public void setEventStatusService(EventStatusService eventStatusService) {
        this.eventStatusService = eventStatusService;
    }

    @Autowired
    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @Autowired
    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
}
