package com.lottery.lotterysystem.service.lotteryEvent;

import com.lottery.lotterysystem.entity.EventStatusEntity;

public interface EventStatusService {
    EventStatusEntity findByName(String name);
}
