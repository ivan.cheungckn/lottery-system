package com.lottery.lotterysystem.emailTemplate;

import lombok.Data;

@Data
public abstract class EmailTemplate {
    private String subject;
    private String body;

    public EmailTemplate(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }
}
