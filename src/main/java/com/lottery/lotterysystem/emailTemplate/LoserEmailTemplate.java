package com.lottery.lotterysystem.emailTemplate;

public class LoserEmailTemplate extends EmailTemplate {
    private String subject;
    private String body;

    public LoserEmailTemplate() {
        super("Sorry", "You are not picked in this event");
    }
}
