package com.lottery.lotterysystem.emailTemplate;

public class WinnerEmailTemplate extends EmailTemplate {

    public WinnerEmailTemplate() {
        super("Congratulations", "You are the winner");
    }
}
