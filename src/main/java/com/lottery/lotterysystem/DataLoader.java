package com.lottery.lotterysystem;

import com.lottery.lotterysystem.entity.UserEntity;
import com.lottery.lotterysystem.service.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
@AllArgsConstructor
public class DataLoader implements ApplicationRunner {
    private UserService userservice;

    public void run(ApplicationArguments args) {
        try {
            userservice.saveUser(new UserEntity(1L, "admin", "admin", "admin", "ivan.cheungckn@gmail.com", new ArrayList<>(), new Date(), new Date()));
        } catch (Exception e) {
        }
        try {
            userservice.addRoleToUser("admin", "admin");
        } catch (Exception e) {
        }

    }
}
