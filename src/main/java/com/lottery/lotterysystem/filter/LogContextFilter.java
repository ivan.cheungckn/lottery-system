package com.lottery.lotterysystem.filter;

import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.UUID;

import static com.lottery.lotterysystem.constant.CoreConstants.TRANSACTION_ID_FOR_LOG;

@Component
@Order(0)
public class LogContextFilter implements Filter {

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        String transactionId = UUID.randomUUID().toString();

        MDC.put(TRANSACTION_ID_FOR_LOG, transactionId);
        try {
            filterChain.doFilter(request, response);
        } finally {
            MDC.clear();
        }
    }
}
