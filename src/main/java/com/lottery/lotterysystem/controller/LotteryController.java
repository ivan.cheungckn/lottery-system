package com.lottery.lotterysystem.controller;

import com.lottery.lotterysystem.dto.LotteryRequestData;
import com.lottery.lotterysystem.exception.DrawStrategyNotFound;
import com.lottery.lotterysystem.service.lottery.LotteryService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/lottery")
@AllArgsConstructor
public class LotteryController {

    private final LotteryService lotteryService;

    @PostMapping
    ResponseEntity createLottery(@RequestBody LotteryRequestData lotteryRequestData) throws DrawStrategyNotFound {
        return ResponseEntity.ok().body(lotteryService.createLotteryAndLotteryEvent(lotteryRequestData));
    }
}
