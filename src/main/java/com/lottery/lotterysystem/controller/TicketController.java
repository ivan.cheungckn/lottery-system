package com.lottery.lotterysystem.controller;

import com.lottery.lotterysystem.dto.GeneralDataResponse;
import com.lottery.lotterysystem.service.ticket.TicketService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/ticket")
@AllArgsConstructor
public class TicketController {

    private TicketService ticketService;

    @GetMapping(value = "/all")
    public ResponseEntity getMyTickets() {
        return ResponseEntity.ok().body(new GeneralDataResponse(true, ticketService.findAllMyTickets()));
    }
}
