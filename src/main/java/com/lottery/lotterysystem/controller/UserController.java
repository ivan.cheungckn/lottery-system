package com.lottery.lotterysystem.controller;

import com.lottery.lotterysystem.dto.CreateUserRequestData;
import com.lottery.lotterysystem.dto.GeneralResponse;
import com.lottery.lotterysystem.entity.UserEntity;
import com.lottery.lotterysystem.service.user.UserService;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/v1/user")
@AllArgsConstructor
public class UserController {
    final Logger log = LogManager.getLogger(UserController.class);

    private final UserService userService;

    @PostMapping
    public ResponseEntity createUser(@RequestBody CreateUserRequestData createUserRequestData) {
        try {
            UserEntity userEntity = userService.createUser(createUserRequestData);
            if (Objects.nonNull(userEntity) && userEntity.getId() > 0) {
                return ResponseEntity.status(CREATED).body(new GeneralResponse(true, "Successfully Created User"));
            } else {
                return ResponseEntity.badRequest().body(new GeneralResponse(false, "Fail to create user"));
            }
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.badRequest().body(new GeneralResponse(false, "User already exists"));
        }
    }
}
