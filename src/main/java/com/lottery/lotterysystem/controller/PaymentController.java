package com.lottery.lotterysystem.controller;

import com.lottery.lotterysystem.dto.GeneralResponse;
import com.lottery.lotterysystem.entity.PaymentTransactionEntity;
import com.lottery.lotterysystem.enums.PaymentType;
import com.lottery.lotterysystem.exception.EventTicketNotSellException;
import com.lottery.lotterysystem.exception.NotAllowedException;
import com.lottery.lotterysystem.exception.PaymentException;
import com.lottery.lotterysystem.service.payment.StripePaymentService;
import com.lottery.lotterysystem.service.ticket.TicketService;
import com.mchange.util.AlreadyExistsException;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/payment")
@AllArgsConstructor
public class PaymentController {
    private final TicketService ticketService;
    private final StripePaymentService stripePaymentService;

    @PostMapping("/lottery-event/{eventId}/stripe")
    public ResponseEntity buyTicket(@PathVariable("eventId") Long eventId) throws AlreadyExistsException, EventTicketNotSellException, PaymentException, NotAllowedException {
        PaymentTransactionEntity paymentTransactionEntity = ticketService.buyTicket(eventId, PaymentType.STRIPE.toString());
        return ResponseEntity.ok().body(stripePaymentService.getResponseData(paymentTransactionEntity));
    }

    @GetMapping("/stripe/callback")
    public ResponseEntity stripeCallback(@RequestParam String paymentIntent) throws AlreadyExistsException, EventTicketNotSellException, PaymentException {
        boolean isSuccess = stripePaymentService.handlePaymentIntentCallback(paymentIntent);
        if (isSuccess) {
            return ResponseEntity.ok().body(new GeneralResponse(true, "Success handle stripe callback"));
        } else {
            throw new PaymentException("Fail to handle stripe callback");
        }
    }
}
