package com.lottery.lotterysystem.controller;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.lottery.lotterysystem.dto.TokenResponse;
import com.lottery.lotterysystem.entity.UserEntity;
import com.lottery.lotterysystem.exception.UnauthorizedException;
import com.lottery.lotterysystem.service.user.UserService;
import com.lottery.lotterysystem.util.JWTUtil;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/api/v1/token")
@AllArgsConstructor
public class TokenController {
    final Logger log = LogManager.getLogger(TokenController.class);

    private UserService userService;

    @GetMapping(value = "/refreshtoken")
    public ResponseEntity refreshToken(HttpServletRequest request, HttpServletResponse response) throws UnauthorizedException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (Objects.nonNull(authorizationHeader) && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refreshToken = authorizationHeader.substring("Bearer ".length());
                DecodedJWT decodedJWT = JWTUtil.verifyToken(refreshToken);
                String username = decodedJWT.getSubject();
                UserEntity userEntity = userService.getUser(username);
                String accessToken = JWTUtil.generateAccessToken(userEntity, request.getRequestURL().toString());
                return ResponseEntity.ok(new TokenResponse(accessToken, refreshToken));
            } catch (Exception e) {
                log.error("Error logging in {}", e.getMessage());
                throw new UnauthorizedException();
            }
        }
        return ResponseEntity.badRequest().body("Fail to refresh token");
    }
}
