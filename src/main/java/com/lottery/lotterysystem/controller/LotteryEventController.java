package com.lottery.lotterysystem.controller;

import com.lottery.lotterysystem.dto.GeneralDataResponse;
import com.lottery.lotterysystem.entity.LotteryEventEntity;
import com.lottery.lotterysystem.service.lotteryEvent.LotteryEventService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/lottery-event")
@AllArgsConstructor
public class LotteryEventController {

    private final LotteryEventService lotteryEventService;

    @GetMapping("/all/active")
    public ResponseEntity getAllOnGoingLotteryEvent() {
        GeneralDataResponse<List<LotteryEventEntity>> response = new GeneralDataResponse<>(true, lotteryEventService.getOnGoingLotteryEvents());
        return ResponseEntity.ok().body(response);
    }
}
