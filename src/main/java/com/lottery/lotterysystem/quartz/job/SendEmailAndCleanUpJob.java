package com.lottery.lotterysystem.quartz.job;

import com.lottery.lotterysystem.dto.EmailRequestData;
import com.lottery.lotterysystem.emailTemplate.EmailTemplate;
import com.lottery.lotterysystem.emailTemplate.LoserEmailTemplate;
import com.lottery.lotterysystem.emailTemplate.WinnerEmailTemplate;
import com.lottery.lotterysystem.entity.*;
import com.lottery.lotterysystem.enums.EventStatus;
import com.lottery.lotterysystem.service.lotteryEvent.EventStatusService;
import com.lottery.lotterysystem.service.lotteryEvent.LotteryEventService;
import com.lottery.lotterysystem.service.notification.EmailService;
import com.lottery.lotterysystem.service.ticket.TicketService;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

@DisallowConcurrentExecution
@AllArgsConstructor
@Component
public class SendEmailAndCleanUpJob extends QuartzJobBean {

    final Logger log = LogManager.getLogger(SendEmailAndCleanUpJob.class);
    private final LotteryEventService lotteryEventService;
    private final EventStatusService eventStatusService;
    private final EmailService emailService;
    private final TicketService ticketService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        //find drawn status lottery event
        //find its users
        //email them
        //save per batch
        EventStatusEntity eventStatusEntity = eventStatusService.findByName(EventStatus.DRAWN.toString());
        if (Objects.isNull(eventStatusEntity)) {
            log.error("Event status {} is not found.", EventStatus.DRAWN);
            return;
        }
        EmailTemplate winnerTemplate = new WinnerEmailTemplate();
        EmailTemplate loserTemplate = new LoserEmailTemplate();
        log.info("sending email and clean up ticketes");
        List<LotteryEventEntity> lotteryEventEntityList = lotteryEventService.findAllByStatus(eventStatusEntity);
        lotteryEventEntityList.forEach(lotteryEventEntity -> {
            UserEntity winnerUserEntity = getWinner(lotteryEventEntity);
            List<TicketEntity> ticketEntitiesNotYetEmail = ticketService.findAllNotYetSendEmail(lotteryEventEntity);
            AtomicBoolean isError = new AtomicBoolean(false);
            ticketEntitiesNotYetEmail.forEach(ticketEntity -> {
                try {
                    UserEntity userEntity = ticketEntity.getUserEntity();
                    EmailRequestData emailRequestData = new EmailRequestData();
                    emailRequestData.setToEmail(userEntity.getEmail());
                    if (Objects.nonNull(winnerUserEntity) && isWinner(userEntity, winnerUserEntity)) {
                        emailRequestData.setSubject(winnerTemplate.getSubject());
                        emailRequestData.setBody(winnerTemplate.getBody());
                    } else {
                        emailRequestData.setSubject(loserTemplate.getSubject());
                        emailRequestData.setBody(loserTemplate.getBody());
                    }
                    EmailEntity emailEntity = emailService.sendEmail(emailRequestData);
                    ticketEntity.setEmailEntity(emailEntity);
                    ticketService.saveTicket(ticketEntity);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    isError.set(true);
                }
            });
            //complete
            if (!isError.get()) {
                lotteryEventService.completeLotteryEventAndDiscardTickets(lotteryEventEntity);
            }
        });
    }

    private UserEntity getWinner(LotteryEventEntity lotteryEventEntity) {
        if (Objects.nonNull(lotteryEventEntity.getWinnerTicketEntity())) {
            return lotteryEventEntity.getWinnerTicketEntity().getUserEntity();
        } else {
            return null;
        }
    }

    private boolean isWinner(UserEntity userEntity, UserEntity winnerUserEntity) {
        return userEntity.getId().compareTo(winnerUserEntity.getId()) == 0;
    }
}
