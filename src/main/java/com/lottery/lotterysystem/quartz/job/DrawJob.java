package com.lottery.lotterysystem.quartz.job;

import com.lottery.lotterysystem.entity.LotteryEventEntity;
import com.lottery.lotterysystem.enums.EventStatus;
import com.lottery.lotterysystem.exception.NotAllowedException;
import com.lottery.lotterysystem.service.drawStrategy.DrawStrategyService;
import com.lottery.lotterysystem.service.lotteryEvent.LotteryEventService;
import com.lottery.lotterysystem.service.payment.PaymentTransactionService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

import static com.lottery.lotterysystem.constant.CoreConstants.DRAW_TRIGGER_GROUP_ID;

@AllArgsConstructor
@DisallowConcurrentExecution
@Component
public class DrawJob extends QuartzJobBean {
    final Logger log = LogManager.getLogger(DrawJob.class);
    private final LotteryEventService lotteryEventService;
    private final DrawStrategyService drawStrategyService;
    private final PaymentTransactionService paymentTransactionService;
    private final long five_minutes_in_millisecond = 5 * 60 * 1000;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        Long eventId = (Long) context.getJobDetail().getJobDataMap().get("eventId");
        Optional<LotteryEventEntity> lotteryEventEntity = lotteryEventService.findById(eventId);
        lotteryEventEntity.ifPresent(entity -> {
            // disable all payment intent
            try {
                log.info("Disabling all payment intent");
                paymentTransactionService.updatePendingPaymentBeforeDrawByEventId(eventId);
                log.info("in drawing");
                boolean drawResult = drawStrategyService.draw(entity);
                if (BooleanUtils.isFalse(drawResult) && isLotteryEventOpen(entity)) {
                    throw new NotAllowedException("Not valid to draw");
                }
                if (!isLotteryEventOpen(entity)) {
                    //lottery event is drawn winner or passed
                    //create a new lottery event
                    lotteryEventService.createLotteryEvent(entity.getLotteryEntity());
                }
                log.info("Draw cronjob for event {} is ended", eventId);

            } catch (Exception e) {
                //postpone to retry
                log.info("rescheduling in event {} 's draw", eventId, e);
                long currentTimeMillisecond = new Date().getTime();
                lotteryEventService.updateScheduleLotteryEvent(entity, DRAW_TRIGGER_GROUP_ID, new Date(currentTimeMillisecond + five_minutes_in_millisecond));
            }
        });
    }

    private boolean isLotteryEventOpen(LotteryEventEntity lotteryEventEntity) {
        return StringUtils.equalsIgnoreCase(lotteryEventEntity.getEventStatusEntity().getName(), EventStatus.OPEN.toString());
    }
}
