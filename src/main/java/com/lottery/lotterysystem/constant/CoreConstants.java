package com.lottery.lotterysystem.constant;

public class CoreConstants {
    public static final String TRANSACTION_ID_FOR_LOG = "TRANS_ID_FOR_LOG";
    public static final String WINNER_EMAIL = "WINNER_EMAIL";
    public static final String LOSER_EMAIL = "LOSER_EMAIL";
    public static final String DRAW_TRIGGER_GROUP_ID = "draw-triggers";
    public static final String DRAW_JOB_DETAIL_GROUP_ID = "draw-jobs";
}
