package com.lottery.lotterysystem.enums;

public enum PaymentStatus {
    COMPLETE("complete"), PENDING("pending"), CANCEL("cancel");
    private String name;

    private PaymentStatus(String _name) {
        name = _name;
    }

    @Override
    public String toString() {
        return name;
    }
}
