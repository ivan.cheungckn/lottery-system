package com.lottery.lotterysystem.enums;

public enum RoleType {
    ADMIN("admin"), USER("user");
    private String name;

    private RoleType(String _name) {
        name = _name;
    }

    @Override
    public String toString() {
        return name;
    }
}
