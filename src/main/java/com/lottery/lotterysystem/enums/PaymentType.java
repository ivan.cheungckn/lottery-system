package com.lottery.lotterysystem.enums;

public enum PaymentType {
    STRIPE("stripe");
    private String name;

    private PaymentType(String _name) {
        name = _name;
    }

    @Override
    public String toString() {
        return name;
    }
}
