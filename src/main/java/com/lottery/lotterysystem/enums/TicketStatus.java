package com.lottery.lotterysystem.enums;

public enum TicketStatus {
    PAID("paid"), PENDING("pending"), CANCEL("cancel");
    private String name;

    private TicketStatus(String _name) {
        name = _name;
    }

    @Override
    public String toString() {
        return name;
    }
}
