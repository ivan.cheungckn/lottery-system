package com.lottery.lotterysystem.enums;

public enum StripePaymentStatus {
    REQUIRES_PAYMENT_METHOD("requires_payment_method"), REQUIRES_CONFIRMATION("requires_confirmation"),
    REQUIRES_ACTION("requires_action"), PROCESSING("processing"), REQUIRES_CAPTURE("requires_capture"),
    CANCELED("canceled"), SUCCEEDED("succeeded");
    private String name;

    private StripePaymentStatus(String _name) {
        name = _name;
    }

    @Override
    public String toString() {
        return name;
    }
}
