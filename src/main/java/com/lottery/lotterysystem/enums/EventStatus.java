package com.lottery.lotterysystem.enums;

public enum EventStatus {
    OPEN("open"), DRAWN("drawn"), SENDING_NOTIFICATION("sending_notification"), COMPLETE("complete");
    private String name;

    private EventStatus(String _name) {
        name = _name;
    }

    @Override
    public String toString() {
        return name;
    }
}
