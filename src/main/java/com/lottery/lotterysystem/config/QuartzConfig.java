package com.lottery.lotterysystem.config;

import com.lottery.lotterysystem.quartz.job.SendEmailAndCleanUpJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {

    @Bean
    public JobDetail SendEmailAndCleanUpJobDetail() {
        return JobBuilder.newJob(SendEmailAndCleanUpJob.class)
                .withIdentity("send_email_and_clean_up_job", "send_email_and_clean_up_job")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger SendEmailAndCleanUpTrigger() {
        return TriggerBuilder.newTrigger()
                .forJob(SendEmailAndCleanUpJobDetail())
                .withIdentity("send_email_and_clean_up_job", "send_email_and_clean_up_job")
                .startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule("0 */2 * ? * *"))
                .build();
    }
}