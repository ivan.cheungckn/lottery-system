package com.lottery.lotterysystem.entityListener;


import com.lottery.lotterysystem.entity.LotteryEventEntity;
import com.lottery.lotterysystem.service.lotteryEvent.LotteryEventService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;

import static com.lottery.lotterysystem.constant.CoreConstants.DRAW_JOB_DETAIL_GROUP_ID;
import static com.lottery.lotterysystem.constant.CoreConstants.DRAW_TRIGGER_GROUP_ID;

@Component
public class LotteryEventEntityListener {

    final Logger log = LogManager.getLogger(LotteryEventEntityListener.class);

    @Autowired
    @Lazy
    private LotteryEventService lotteryEventService;

    @PostPersist
    public void afterCreate(LotteryEventEntity lotteryEventEntity) {
        lotteryEventService.createScheduleLotteryEvent(lotteryEventEntity, DRAW_JOB_DETAIL_GROUP_ID, DRAW_TRIGGER_GROUP_ID);
    }

}
