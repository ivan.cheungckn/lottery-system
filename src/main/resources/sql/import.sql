-- ### In your Quartz properties file, youll need to set
-- # org.quartz.jobStore.driverDelegateClass = org.quartz.impl.jdbcjobstore.StdJDBCDelegate
-- #
-- #
-- # By: Ron Cordell - roncordell
-- #  I didn't see this anywhere, so I thought I'd post it here. This is the script from Quartz to create the tables in a MySQL database, modified to use INNODB instead of MYISAM.

DROP TABLE IF EXISTS QRTZ_FIRED_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_PAUSED_TRIGGER_GRPS;
DROP TABLE IF EXISTS QRTZ_SCHEDULER_STATE;
DROP TABLE IF EXISTS QRTZ_LOCKS;
DROP TABLE IF EXISTS QRTZ_SIMPLE_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_SIMPROP_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_CRON_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_BLOB_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_JOB_DETAILS;
DROP TABLE IF EXISTS QRTZ_CALENDARS;

CREATE TABLE QRTZ_JOB_DETAILS
(
    SCHED_NAME        VARCHAR(120) NOT NULL,
    JOB_NAME          VARCHAR(190) NOT NULL,
    JOB_GROUP         VARCHAR(190) NOT NULL,
    DESCRIPTION       VARCHAR(250) NULL,
    JOB_CLASS_NAME    VARCHAR(250) NOT NULL,
    IS_DURABLE        VARCHAR(1)   NOT NULL,
    IS_NONCONCURRENT  VARCHAR(1)   NOT NULL,
    IS_UPDATE_DATA    VARCHAR(1)   NOT NULL,
    REQUESTS_RECOVERY VARCHAR(1)   NOT NULL,
    JOB_DATA          BLOB NULL,
    PRIMARY KEY (SCHED_NAME, JOB_NAME, JOB_GROUP)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_TRIGGERS
(
    SCHED_NAME     VARCHAR(120) NOT NULL,
    TRIGGER_NAME   VARCHAR(190) NOT NULL,
    TRIGGER_GROUP  VARCHAR(190) NOT NULL,
    JOB_NAME       VARCHAR(190) NOT NULL,
    JOB_GROUP      VARCHAR(190) NOT NULL,
    DESCRIPTION    VARCHAR(250) NULL,
    NEXT_FIRE_TIME BIGINT(13) NULL,
    PREV_FIRE_TIME BIGINT(13) NULL,
    PRIORITY       INTEGER NULL,
    TRIGGER_STATE  VARCHAR(16)  NOT NULL,
    TRIGGER_TYPE   VARCHAR(8)   NOT NULL,
    START_TIME     BIGINT(13) NOT NULL,
    END_TIME       BIGINT(13) NULL,
    CALENDAR_NAME  VARCHAR(190) NULL,
    MISFIRE_INSTR  SMALLINT(2) NULL,
    JOB_DATA       BLOB NULL,
    PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME, JOB_NAME, JOB_GROUP)
        REFERENCES QRTZ_JOB_DETAILS (SCHED_NAME, JOB_NAME, JOB_GROUP)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_SIMPLE_TRIGGERS
(
    SCHED_NAME      VARCHAR(120) NOT NULL,
    TRIGGER_NAME    VARCHAR(190) NOT NULL,
    TRIGGER_GROUP   VARCHAR(190) NOT NULL,
    REPEAT_COUNT    BIGINT(7) NOT NULL,
    REPEAT_INTERVAL BIGINT(12) NOT NULL,
    TIMES_TRIGGERED BIGINT(10) NOT NULL,
    PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_CRON_TRIGGERS
(
    SCHED_NAME      VARCHAR(120) NOT NULL,
    TRIGGER_NAME    VARCHAR(190) NOT NULL,
    TRIGGER_GROUP   VARCHAR(190) NOT NULL,
    CRON_EXPRESSION VARCHAR(120) NOT NULL,
    TIME_ZONE_ID    VARCHAR(80),
    PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_SIMPROP_TRIGGERS
(
    SCHED_NAME    VARCHAR(120) NOT NULL,
    TRIGGER_NAME  VARCHAR(190) NOT NULL,
    TRIGGER_GROUP VARCHAR(190) NOT NULL,
    STR_PROP_1    VARCHAR(512) NULL,
    STR_PROP_2    VARCHAR(512) NULL,
    STR_PROP_3    VARCHAR(512) NULL,
    INT_PROP_1    INT NULL,
    INT_PROP_2    INT NULL,
    LONG_PROP_1   BIGINT NULL,
    LONG_PROP_2   BIGINT NULL,
    DEC_PROP_1    NUMERIC(13, 4) NULL,
    DEC_PROP_2    NUMERIC(13, 4) NULL,
    BOOL_PROP_1   VARCHAR(1) NULL,
    BOOL_PROP_2   VARCHAR(1) NULL,
    PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_BLOB_TRIGGERS
(
    SCHED_NAME    VARCHAR(120) NOT NULL,
    TRIGGER_NAME  VARCHAR(190) NOT NULL,
    TRIGGER_GROUP VARCHAR(190) NOT NULL,
    BLOB_DATA     BLOB NULL,
    PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    INDEX (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_CALENDARS
(
    SCHED_NAME    VARCHAR(120) NOT NULL,
    CALENDAR_NAME VARCHAR(190) NOT NULL,
    CALENDAR      BLOB         NOT NULL,
    PRIMARY KEY (SCHED_NAME, CALENDAR_NAME)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_PAUSED_TRIGGER_GRPS
(
    SCHED_NAME    VARCHAR(120) NOT NULL,
    TRIGGER_GROUP VARCHAR(190) NOT NULL,
    PRIMARY KEY (SCHED_NAME, TRIGGER_GROUP)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_FIRED_TRIGGERS
(
    SCHED_NAME        VARCHAR(120) NOT NULL,
    ENTRY_ID          VARCHAR(95)  NOT NULL,
    TRIGGER_NAME      VARCHAR(190) NOT NULL,
    TRIGGER_GROUP     VARCHAR(190) NOT NULL,
    INSTANCE_NAME     VARCHAR(190) NOT NULL,
    FIRED_TIME        BIGINT(13) NOT NULL,
    SCHED_TIME        BIGINT(13) NOT NULL,
    PRIORITY          INTEGER      NOT NULL,
    STATE             VARCHAR(16)  NOT NULL,
    JOB_NAME          VARCHAR(190) NULL,
    JOB_GROUP         VARCHAR(190) NULL,
    IS_NONCONCURRENT  VARCHAR(1) NULL,
    REQUESTS_RECOVERY VARCHAR(1) NULL,
    PRIMARY KEY (SCHED_NAME, ENTRY_ID)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_SCHEDULER_STATE
(
    SCHED_NAME        VARCHAR(120) NOT NULL,
    INSTANCE_NAME     VARCHAR(190) NOT NULL,
    LAST_CHECKIN_TIME BIGINT(13) NOT NULL,
    CHECKIN_INTERVAL  BIGINT(13) NOT NULL,
    PRIMARY KEY (SCHED_NAME, INSTANCE_NAME)
) ENGINE=InnoDB;

CREATE TABLE QRTZ_LOCKS
(
    SCHED_NAME VARCHAR(120) NOT NULL,
    LOCK_NAME  VARCHAR(40)  NOT NULL,
    PRIMARY KEY (SCHED_NAME, LOCK_NAME)
) ENGINE=InnoDB;

CREATE
INDEX IDX_QRTZ_J_REQ_RECOVERY ON QRTZ_JOB_DETAILS(SCHED_NAME,REQUESTS_RECOVERY);
CREATE
INDEX IDX_QRTZ_J_GRP ON QRTZ_JOB_DETAILS(SCHED_NAME,JOB_GROUP);

CREATE
INDEX IDX_QRTZ_T_J ON QRTZ_TRIGGERS(SCHED_NAME,JOB_NAME,JOB_GROUP);
CREATE
INDEX IDX_QRTZ_T_JG ON QRTZ_TRIGGERS(SCHED_NAME,JOB_GROUP);
CREATE
INDEX IDX_QRTZ_T_C ON QRTZ_TRIGGERS(SCHED_NAME,CALENDAR_NAME);
CREATE
INDEX IDX_QRTZ_T_G ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_GROUP);
CREATE
INDEX IDX_QRTZ_T_STATE ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_STATE);
CREATE
INDEX IDX_QRTZ_T_N_STATE ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_STATE);
CREATE
INDEX IDX_QRTZ_T_N_G_STATE ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_GROUP,TRIGGER_STATE);
CREATE
INDEX IDX_QRTZ_T_NEXT_FIRE_TIME ON QRTZ_TRIGGERS(SCHED_NAME,NEXT_FIRE_TIME);
CREATE
INDEX IDX_QRTZ_T_NFT_ST ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_STATE,NEXT_FIRE_TIME);
CREATE
INDEX IDX_QRTZ_T_NFT_MISFIRE ON QRTZ_TRIGGERS(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME);
CREATE
INDEX IDX_QRTZ_T_NFT_ST_MISFIRE ON QRTZ_TRIGGERS(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_STATE);
CREATE
INDEX IDX_QRTZ_T_NFT_ST_MISFIRE_GRP ON QRTZ_TRIGGERS(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_GROUP,TRIGGER_STATE);

CREATE
INDEX IDX_QRTZ_FT_TRIG_INST_NAME ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,INSTANCE_NAME);
CREATE
INDEX IDX_QRTZ_FT_INST_JOB_REQ_RCVRY ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,INSTANCE_NAME,REQUESTS_RECOVERY);
CREATE
INDEX IDX_QRTZ_FT_J_G ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,JOB_NAME,JOB_GROUP);
CREATE
INDEX IDX_QRTZ_FT_JG ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,JOB_GROUP);
CREATE
INDEX IDX_QRTZ_FT_T_G ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP);
CREATE
INDEX IDX_QRTZ_FT_TG ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,TRIGGER_GROUP);

commit;


create table draw_startegy
(
    id            bigint       not null auto_increment,
    created_date  datetime(6) not null,
    description   varchar(255),
    modified_date datetime(6),
    type          varchar(255) not null,
    primary key (id)
) engine=InnoDB;

create table email
(
    id            bigint not null auto_increment,
    content       varchar(255),
    created_date  datetime(6) not null,
    modified_date datetime(6),
    subject       varchar(255),
    toEmail       varchar(255),
    ticket_id     bigint,
    primary key (id)
) engine=InnoDB;

create table event_status
(
    id            bigint not null auto_increment,
    created_date  datetime(6) not null,
    modified_date datetime(6),
    name          varchar(255),
    primary key (id)
) engine=InnoDB;

create table lottery
(
    id               bigint not null auto_increment,
    created_date     datetime(6) not null,
    description      varchar(255),
    end_time         datetime(6),
    interval_second  bigint,
    is_active        bit    not null,
    modified_date    datetime(6),
    name             varchar(255),
    start_time       datetime(6),
    ticket_price     bigint,
    draw_strategy_id bigint,
    primary key (id)
) engine=InnoDB;

create table lottery_event
(
    id               bigint not null auto_increment,
    created_date     datetime(6) not null,
    end_time         datetime(6),
    modified_date    datetime(6),
    start_time       datetime(6),
    event_status_id  bigint,
    lottery_id       bigint,
    winner_ticket_id bigint,
    primary key (id)
) engine=InnoDB;

create table payment
(
    id                     bigint not null auto_increment,
    payment_transaction_id bigint,
    primary key (id)
) engine=InnoDB;

create table payment_status
(
    id            bigint not null auto_increment,
    created_date  datetime(6) not null,
    modified_date datetime(6),
    name          varchar(255),
    primary key (id)
) engine=InnoDB;

create table payment_transaction
(
    id                bigint not null auto_increment,
    created_date      datetime(6) not null,
    modified_date     datetime(6),
    payment_status_id bigint,
    payment_type_id   bigint,
    ticket_id         bigint,
    primary key (id)
) engine=InnoDB;

create table payment_type
(
    id            bigint not null auto_increment,
    created_date  datetime(6) not null,
    modified_date datetime(6),
    name          varchar(255),
    primary key (id)
) engine=InnoDB;

create table role
(
    id            bigint not null auto_increment,
    created_date  datetime(6) not null,
    modified_date datetime(6),
    name          varchar(255),
    primary key (id)
) engine=InnoDB;

create table stripe_payment
(
    client_secret  varchar(255),
    created_date   datetime(6) not null,
    modified_date  datetime(6),
    payment_intent varchar(255),
    id             bigint not null,
    primary key (id)
) engine=InnoDB;

create table ticket
(
    id               bigint not null auto_increment,
    created_date     datetime(6) not null,
    modified_date    datetime(6),
    email_id         bigint,
    lottery_event_id bigint,
    status_id        bigint,
    user_id          bigint,
    primary key (id)
) engine=InnoDB;

create table ticket_status
(
    id            bigint not null auto_increment,
    created_date  datetime(6) not null,
    modified_date datetime(6),
    name          varchar(255),
    primary key (id)
) engine=InnoDB;

create table user
(
    id            bigint not null auto_increment,
    created_date  datetime(6) not null,
    email         varchar(255),
    modified_date datetime(6),
    name          varchar(255),
    password      varchar(255),
    username      varchar(255),
    primary key (id)
) engine=InnoDB;

create table user_role
(
    user_id bigint not null,
    role_id bigint not null
) engine=InnoDB;
create
index IDX6ve3g4ilxq0c8fb26ctia7h7d on draw_startegy (type);
create
index IDXj6qrnoqxew9hbx7hwth3yoli8 on event_status (name);

alter table event_status
    add constraint UK_j6qrnoqxew9hbx7hwth3yoli8 unique (name);
create
index IDXs1b8xa32w50stydbe7nmjgbin on payment_status (name);

alter table payment_status
    add constraint UK_s1b8xa32w50stydbe7nmjgbin unique (name);
create
index IDXfh0llcvj1s3dpypyjqsa4fi6o on payment_type (name);

alter table payment_type
    add constraint UK_fh0llcvj1s3dpypyjqsa4fi6o unique (name);
create
index IDX8sewwnpamngi6b1dwaa88askk on role (name);

alter table role
    add constraint UK_8sewwnpamngi6b1dwaa88askk unique (name);

alter table stripe_payment
    add constraint UKiaw99d3cqln5g3vuuxpojk7d0 unique (client_secret);

alter table stripe_payment
    add constraint UKn16ou05d9jc2gyopgd19itysv unique (payment_intent);

alter table ticket
    add constraint UKwohro0pxb9sw1lde1uc4c3ip unique (lottery_event_id, user_id);
create
index IDX35mdbsrabdvdqqwh7qsixncdh on ticket_status (name);

alter table ticket_status
    add constraint UK_35mdbsrabdvdqqwh7qsixncdh unique (name);
create
index IDXsb8bbouer5wak8vyiiy4pf2bx on user (username);

alter table user
    add constraint UK_sb8bbouer5wak8vyiiy4pf2bx unique (username);

alter table email
    add constraint FKqh55053f2gp5vk506bfb3qeq5
        foreign key (ticket_id)
            references ticket (id);

alter table lottery
    add constraint FKp9b4g0qu8vrnj60wcux42yq3e
        foreign key (draw_strategy_id)
            references draw_startegy (id);

alter table lottery_event
    add constraint FK7252ls7dh1f2t42fg3c8c6rpp
        foreign key (event_status_id)
            references event_status (id);

alter table lottery_event
    add constraint FKmfgtlbkaf0ilwmm5l66naanlo
        foreign key (lottery_id)
            references lottery (id);

alter table lottery_event
    add constraint FKm42hk77cbnenqyvhf0xpwakje
        foreign key (winner_ticket_id)
            references ticket (id);

alter table payment
    add constraint FKete3ks6af0bmjdehxh56h6bu6
        foreign key (payment_transaction_id)
            references payment_transaction (id);

alter table payment_transaction
    add constraint FKp4ehxq7h9axnubst90j09lh3p
        foreign key (payment_status_id)
            references payment_status (id);

alter table payment_transaction
    add constraint FK7t4veye8lvhcp2xmdh9pd3lde
        foreign key (payment_type_id)
            references payment_type (id);

alter table payment_transaction
    add constraint FKc8vyplfkvpipmcx118hapcla8
        foreign key (ticket_id)
            references ticket (id);

alter table stripe_payment
    add constraint FKlrmxxt9ap2k3dkweeasydccb3
        foreign key (id)
            references payment (id);

alter table ticket
    add constraint FKlujsa37fy12m68d44dv7uym44
        foreign key (email_id)
            references email (id);

alter table ticket
    add constraint FKeher230txt70gncqcdhtwdwip
        foreign key (lottery_event_id)
            references lottery_event (id);

alter table ticket
    add constraint FKf5v1wmegho829dbtu7t2pwm1q
        foreign key (status_id)
            references ticket_status (id);

alter table ticket
    add constraint FKdvt57mcco3ogsosi97odw563o
        foreign key (user_id)
            references user (id);

alter table user_role
    add constraint FKa68196081fvovjhkek5m97n3y
        foreign key (role_id)
            references role (id);

alter table user_role
    add constraint FK859n2jvi8ivhui0rl0esws6o
        foreign key (user_id)
            references user (id);


INSERT INTO payment_status (id, name, created_date, modified_date)
VALUES ('1', 'complete', '2022-1-1', '2022-1-1'),
       ('2', 'pending', '2022-1-1', '2022-1-1'),
       ('3', 'cancel', '2022-1-1', '2022-1-1');

INSERT INTO event_status (id, name, created_date, modified_date)
VALUES ('1', 'open', '2022-1-1', '2022-1-1'),
       ('2', 'drawn', '2022-1-1', '2022-1-1'),
       ('3', 'sending_notification', '2022-1-1', '2022-1-1'),
       ('4', 'complete', '2022-1-1', '2022-1-1');

INSERT INTO payment_type (id, name, created_date, modified_date)
VALUES ('1', 'stripe', '2022-1-1', '2022-1-1');

INSERT INTO ticket_status (id, name, created_date, modified_date)
VALUES ('1', 'pending', '2022-1-1', '2022-1-1'),
       ('2', 'paid', '2022-1-1', '2022-1-1'),
       ('3', 'cancel', '2022-1-1', '2022-1-1');

INSERT INTO role (id, name, created_date, modified_date)
VALUES ('1', 'admin', '2022-1-1', '2022-1-1'),
       ('2', 'user', '2022-1-1', '2022-1-1');

INSERT INTO draw_startegy (id, type, created_date, modified_date)
VALUES ('1', 'random', '2022-1-1', '2022-1-1');

commit;


