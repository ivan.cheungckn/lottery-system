FROM maven:3.8-jdk-11 as builder

# Copy local code to the container image.
WORKDIR /app

COPY ./pom.xml ./pom.xml
# fetch all dependencies
RUN mvn dependency:go-offline -B

COPY ./src ./src

# Build a release artifact.
RUN mvn package -Dmaven.test.skip

FROM adoptopenjdk/openjdk11:jdk-11.0.8_10-alpine

ENV TZ=Asia/Hong_Kong
ENV JAVA_OPTS="-server -verbose:gc"


#RUN apk update && apk add bash
RUN apk add --no-cache bash

ENV LD_LIBRARY_PATH /usr/lib

ARG JAR_FILE=/app/target/*.jar
COPY --from=builder ${JAR_FILE} app.jar

ENTRYPOINT exec java $JAVA_OPTS -jar /app.jar