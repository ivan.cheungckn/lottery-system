# Lottery System

Please checkout the project and run\
`docker-compose up --build`
>port: 8080 for the backend server\
port: 3319 for the database


### admin ac:
ac: admin\
pw: admin

0. register ac
1. crate a lottery ({domain}/api/v1/lottery) (need to use admin ac)
2. check current active event ({domain}/api/v1/lottery-event/all/active)
3. buy ticket for event ({domain}/api/v1/payment/lottery-event/{eventId}/stripe)
4. finish payment (https://api.stripe.com/v1/payment_intents/{paymentIntentId}/confirm) 
5. call back backend server after payment ({domain}/api/v1/payment/stripe/callback?paymentIntent={paymentIntentId})

wait the event end will draw a winner and send email to it by its registered account's email.

ps. postman api doc:
https://www.getpostman.com/collections/1afbffc0deb953e9dbcb

### limitation

- have to wait for cronjob to send email to users. If having a cronjob to search and send response to missed customers
  and at the same time after drawn the winner and send email right after it may cause overlapping. It means may send
  twice to response to users. Maybe add a locking mechanism like redis to atomically to prevent overlap.
- currently, sending email one by one in cronjob and not very robust. use message queue to process email request, since
  it is more robust. eg. if it is failed, it can requeue the message and lesser chance to double-sending response. Also,
  It can let multiple machines to consume the email request at the same time
- difficult to scale up specific part, because like user, lottery, sending email are in the same system. We can Split it
  into 3 microservices to enhance scalability.

